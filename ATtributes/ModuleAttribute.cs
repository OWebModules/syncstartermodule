﻿using SyncStarterModule.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncStarterModule.Attributes
{
    public class ModuleAttribute:Attribute
    {
        public string Module { get; set; }
        public Type[] ParameterTypes { get; set; }
    }
    
}
