﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncStarterModule.Attributes
{
    public enum ResultProp
    {
        Id,
        Name
    }
    public class ParamPropAttribute : Attribute
    {
        public string IdClass { get; set; }
        public string NameClass { get; set; }
        public ResultProp ResultProperty {get; set;}
    }
}
