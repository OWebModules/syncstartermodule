﻿using OntologyAppDBConnector;
using SecurityModule;
using SyncStarterModule.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SyncStarterModule
{
    public partial class frmAuthenticate : Form
    {

        private Globals globals;

        public bool LoggedIn { get { return StaticConfig.SecurityController.IsAuthenticated;  } }

        public frmAuthenticate(Globals globals)
        {
            InitializeComponent();
            this.globals = globals;
        }

        private async void buttonLogin_Click(object sender, EventArgs e)
        {
            StaticConfig.SecurityController = new SecurityController(new OntologyAppDBConnector.Globals());
            var loginResult = await StaticConfig.SecurityController.Login(textBoxEmailAddress.Text, textBoxPassword.Text);
            if (loginResult.Result.GUID == globals.LState_Error.GUID)
            {
                MessageBox.Show("Username or password is wrong!");
                return;
            }
            StaticConfig.MasterPassword = textBoxPassword.Text;

            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void textBoxUserName_TextChanged(object sender, EventArgs e)
        {
            ValidateLoginPre();
        }

        private void ValidateLoginPre()
        {
            buttonLogin.Enabled = false;
            if (!string.IsNullOrEmpty(textBoxEmailAddress.Text) && !string.IsNullOrEmpty(textBoxPassword.Text))
            {
                buttonLogin.Enabled = true;
                return;
            }
        }

        private void textBoxPassword_TextChanged(object sender, EventArgs e)
        {
            ValidateLoginPre();
        }
    }
}
