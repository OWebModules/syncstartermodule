﻿using AutomationLibrary.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using SyncStarterModule.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncStarterModule.Interfaces
{
    public interface IController
    {
        CommandLine CommandLine { get; }

        Globals Globals { get; }

        bool IsValid { get; }

        string ErrorMessage { get; }

        string Syntax { get; }

        clsOntologyItem DoWork();
    }
}
