﻿namespace SyncStarterModule
{
    partial class InitOModules
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn1 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor1 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.groupBoxSecurity = new System.Windows.Forms.GroupBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.labelEmailAddress = new System.Windows.Forms.Label();
            this.pictureBoxPassword = new System.Windows.Forms.PictureBox();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.labelPassword = new System.Windows.Forms.Label();
            this.pictureBoxGroup = new System.Windows.Forms.PictureBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.labelGroup = new System.Windows.Forms.Label();
            this.pictureBoxUserName = new System.Windows.Forms.PictureBox();
            this.textBoxUser = new System.Windows.Forms.TextBox();
            this.labelUser = new System.Windows.Forms.Label();
            this.radGridViewOutput = new Telerik.WinControls.UI.RadGridView();
            this.groupBoxFilesystem = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.labelWatcherPath = new System.Windows.Forms.Label();
            this.buttonAddFolder = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBoxMediaStorePath = new System.Windows.Forms.TextBox();
            this.labelBlobDirPath = new System.Windows.Forms.Label();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBoxMediaStore = new System.Windows.Forms.GroupBox();
            this.pictureBoxPort = new System.Windows.Forms.PictureBox();
            this.textBoxPort = new System.Windows.Forms.TextBox();
            this.labelPort = new System.Windows.Forms.Label();
            this.pictureBoxServer = new System.Windows.Forms.PictureBox();
            this.textBoxServer = new System.Windows.Forms.TextBox();
            this.labelServer = new System.Windows.Forms.Label();
            this.pictureBoxProtocol = new System.Windows.Forms.PictureBox();
            this.textBoxProtocol = new System.Windows.Forms.TextBox();
            this.labelProtocol = new System.Windows.Forms.Label();
            this.groupBoxSecurity.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewOutput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewOutput.MasterTemplate)).BeginInit();
            this.groupBoxFilesystem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBoxMediaStore.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxServer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxProtocol)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonOK
            // 
            this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOK.Location = new System.Drawing.Point(457, 530);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 0;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(538, 530);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 1;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // groupBoxSecurity
            // 
            this.groupBoxSecurity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxSecurity.Controls.Add(this.pictureBox3);
            this.groupBoxSecurity.Controls.Add(this.textBox3);
            this.groupBoxSecurity.Controls.Add(this.labelEmailAddress);
            this.groupBoxSecurity.Controls.Add(this.pictureBoxPassword);
            this.groupBoxSecurity.Controls.Add(this.textBoxPassword);
            this.groupBoxSecurity.Controls.Add(this.labelPassword);
            this.groupBoxSecurity.Controls.Add(this.pictureBoxGroup);
            this.groupBoxSecurity.Controls.Add(this.textBox1);
            this.groupBoxSecurity.Controls.Add(this.labelGroup);
            this.groupBoxSecurity.Controls.Add(this.pictureBoxUserName);
            this.groupBoxSecurity.Controls.Add(this.textBoxUser);
            this.groupBoxSecurity.Controls.Add(this.labelUser);
            this.groupBoxSecurity.Location = new System.Drawing.Point(4, 5);
            this.groupBoxSecurity.Name = "groupBoxSecurity";
            this.groupBoxSecurity.Size = new System.Drawing.Size(618, 121);
            this.groupBoxSecurity.TabIndex = 2;
            this.groupBoxSecurity.TabStop = false;
            this.groupBoxSecurity.Text = "Security";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.Location = new System.Drawing.Point(589, 95);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(25, 20);
            this.pictureBox3.TabIndex = 11;
            this.pictureBox3.TabStop = false;
            // 
            // textBox3
            // 
            this.textBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox3.Location = new System.Drawing.Point(106, 95);
            this.textBox3.Name = "textBox3";
            this.textBox3.PasswordChar = '*';
            this.textBox3.Size = new System.Drawing.Size(477, 20);
            this.textBox3.TabIndex = 10;
            // 
            // labelEmailAddress
            // 
            this.labelEmailAddress.AutoSize = true;
            this.labelEmailAddress.Location = new System.Drawing.Point(5, 98);
            this.labelEmailAddress.Name = "labelEmailAddress";
            this.labelEmailAddress.Size = new System.Drawing.Size(76, 13);
            this.labelEmailAddress.TabIndex = 9;
            this.labelEmailAddress.Text = "Email-Address:";
            // 
            // pictureBoxPassword
            // 
            this.pictureBoxPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxPassword.Location = new System.Drawing.Point(589, 69);
            this.pictureBoxPassword.Name = "pictureBoxPassword";
            this.pictureBoxPassword.Size = new System.Drawing.Size(25, 20);
            this.pictureBoxPassword.TabIndex = 8;
            this.pictureBoxPassword.TabStop = false;
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxPassword.Location = new System.Drawing.Point(106, 69);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.PasswordChar = '*';
            this.textBoxPassword.Size = new System.Drawing.Size(477, 20);
            this.textBoxPassword.TabIndex = 7;
            // 
            // labelPassword
            // 
            this.labelPassword.AutoSize = true;
            this.labelPassword.Location = new System.Drawing.Point(5, 72);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(56, 13);
            this.labelPassword.TabIndex = 6;
            this.labelPassword.Text = "Password:";
            // 
            // pictureBoxGroup
            // 
            this.pictureBoxGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxGroup.Location = new System.Drawing.Point(589, 43);
            this.pictureBoxGroup.Name = "pictureBoxGroup";
            this.pictureBoxGroup.Size = new System.Drawing.Size(25, 20);
            this.pictureBoxGroup.TabIndex = 5;
            this.pictureBoxGroup.TabStop = false;
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(106, 43);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(477, 20);
            this.textBox1.TabIndex = 4;
            // 
            // labelGroup
            // 
            this.labelGroup.AutoSize = true;
            this.labelGroup.Location = new System.Drawing.Point(5, 46);
            this.labelGroup.Name = "labelGroup";
            this.labelGroup.Size = new System.Drawing.Size(39, 13);
            this.labelGroup.TabIndex = 3;
            this.labelGroup.Text = "Group:";
            // 
            // pictureBoxUserName
            // 
            this.pictureBoxUserName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxUserName.Location = new System.Drawing.Point(589, 17);
            this.pictureBoxUserName.Name = "pictureBoxUserName";
            this.pictureBoxUserName.Size = new System.Drawing.Size(25, 20);
            this.pictureBoxUserName.TabIndex = 2;
            this.pictureBoxUserName.TabStop = false;
            // 
            // textBoxUser
            // 
            this.textBoxUser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxUser.Location = new System.Drawing.Point(106, 17);
            this.textBoxUser.Name = "textBoxUser";
            this.textBoxUser.Size = new System.Drawing.Size(477, 20);
            this.textBoxUser.TabIndex = 1;
            // 
            // labelUser
            // 
            this.labelUser.AutoSize = true;
            this.labelUser.Location = new System.Drawing.Point(5, 20);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(32, 13);
            this.labelUser.TabIndex = 0;
            this.labelUser.Text = "User:";
            // 
            // radGridViewOutput
            // 
            this.radGridViewOutput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridViewOutput.BackColor = System.Drawing.SystemColors.Control;
            this.radGridViewOutput.Cursor = System.Windows.Forms.Cursors.Default;
            this.radGridViewOutput.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.radGridViewOutput.ForeColor = System.Drawing.Color.Black;
            this.radGridViewOutput.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.radGridViewOutput.Location = new System.Drawing.Point(4, 325);
            // 
            // 
            // 
            this.radGridViewOutput.MasterTemplate.AllowAddNewRow = false;
            this.radGridViewOutput.MasterTemplate.AllowDeleteRow = false;
            this.radGridViewOutput.MasterTemplate.AllowEditRow = false;
            this.radGridViewOutput.MasterTemplate.AutoGenerateColumns = false;
            this.radGridViewOutput.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewDateTimeColumn1.EnableExpressionEditor = false;
            gridViewDateTimeColumn1.FieldName = "Stamp";
            gridViewDateTimeColumn1.HeaderText = "Stamp";
            gridViewDateTimeColumn1.IsAutoGenerated = true;
            gridViewDateTimeColumn1.Name = "Stamp";
            gridViewDateTimeColumn1.SortOrder = Telerik.WinControls.UI.RadSortOrder.Descending;
            gridViewDateTimeColumn1.Width = 81;
            gridViewTextBoxColumn1.EnableExpressionEditor = false;
            gridViewTextBoxColumn1.FieldName = "Type";
            gridViewTextBoxColumn1.HeaderText = "Type";
            gridViewTextBoxColumn1.IsAutoGenerated = true;
            gridViewTextBoxColumn1.Name = "Type";
            gridViewTextBoxColumn1.Width = 81;
            gridViewTextBoxColumn2.EnableExpressionEditor = false;
            gridViewTextBoxColumn2.FieldName = "Module";
            gridViewTextBoxColumn2.HeaderText = "Module";
            gridViewTextBoxColumn2.IsAutoGenerated = true;
            gridViewTextBoxColumn2.Name = "Module";
            gridViewTextBoxColumn2.Width = 86;
            gridViewTextBoxColumn3.EnableExpressionEditor = false;
            gridViewTextBoxColumn3.FieldName = "Function";
            gridViewTextBoxColumn3.HeaderText = "Function";
            gridViewTextBoxColumn3.IsAutoGenerated = true;
            gridViewTextBoxColumn3.Name = "Function";
            gridViewTextBoxColumn3.Width = 89;
            gridViewTextBoxColumn4.EnableExpressionEditor = false;
            gridViewTextBoxColumn4.FieldName = "Message";
            gridViewTextBoxColumn4.HeaderText = "Message";
            gridViewTextBoxColumn4.IsAutoGenerated = true;
            gridViewTextBoxColumn4.Name = "Message";
            gridViewTextBoxColumn4.Width = 260;
            this.radGridViewOutput.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewDateTimeColumn1,
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4});
            this.radGridViewOutput.MasterTemplate.EnableFiltering = true;
            this.radGridViewOutput.MasterTemplate.SelectionMode = Telerik.WinControls.UI.GridViewSelectionMode.CellSelect;
            sortDescriptor1.Direction = System.ComponentModel.ListSortDirection.Descending;
            sortDescriptor1.PropertyName = "Stamp";
            this.radGridViewOutput.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor1});
            this.radGridViewOutput.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridViewOutput.Name = "radGridViewOutput";
            this.radGridViewOutput.ReadOnly = true;
            this.radGridViewOutput.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radGridViewOutput.Size = new System.Drawing.Size(614, 199);
            this.radGridViewOutput.TabIndex = 4;
            // 
            // groupBoxFilesystem
            // 
            this.groupBoxFilesystem.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxFilesystem.Controls.Add(this.button1);
            this.groupBoxFilesystem.Controls.Add(this.pictureBox2);
            this.groupBoxFilesystem.Controls.Add(this.textBox2);
            this.groupBoxFilesystem.Controls.Add(this.labelWatcherPath);
            this.groupBoxFilesystem.Controls.Add(this.buttonAddFolder);
            this.groupBoxFilesystem.Controls.Add(this.pictureBox1);
            this.groupBoxFilesystem.Controls.Add(this.textBoxMediaStorePath);
            this.groupBoxFilesystem.Controls.Add(this.labelBlobDirPath);
            this.groupBoxFilesystem.Location = new System.Drawing.Point(4, 132);
            this.groupBoxFilesystem.Name = "groupBoxFilesystem";
            this.groupBoxFilesystem.Size = new System.Drawing.Size(618, 77);
            this.groupBoxFilesystem.TabIndex = 5;
            this.groupBoxFilesystem.TabStop = false;
            this.groupBoxFilesystem.Text = "Filesystem";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(552, 41);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(31, 23);
            this.button1.TabIndex = 14;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.Location = new System.Drawing.Point(589, 41);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(25, 23);
            this.pictureBox2.TabIndex = 13;
            this.pictureBox2.TabStop = false;
            // 
            // textBox2
            // 
            this.textBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox2.Location = new System.Drawing.Point(106, 43);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(440, 20);
            this.textBox2.TabIndex = 12;
            // 
            // labelWatcherPath
            // 
            this.labelWatcherPath.AutoSize = true;
            this.labelWatcherPath.Location = new System.Drawing.Point(5, 46);
            this.labelWatcherPath.Name = "labelWatcherPath";
            this.labelWatcherPath.Size = new System.Drawing.Size(76, 13);
            this.labelWatcherPath.TabIndex = 11;
            this.labelWatcherPath.Text = "Watcher-Path:";
            // 
            // buttonAddFolder
            // 
            this.buttonAddFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAddFolder.Location = new System.Drawing.Point(552, 15);
            this.buttonAddFolder.Name = "buttonAddFolder";
            this.buttonAddFolder.Size = new System.Drawing.Size(31, 23);
            this.buttonAddFolder.TabIndex = 10;
            this.buttonAddFolder.Text = "...";
            this.buttonAddFolder.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Location = new System.Drawing.Point(589, 15);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(25, 23);
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // textBoxMediaStorePath
            // 
            this.textBoxMediaStorePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxMediaStorePath.Location = new System.Drawing.Point(106, 17);
            this.textBoxMediaStorePath.Name = "textBoxMediaStorePath";
            this.textBoxMediaStorePath.Size = new System.Drawing.Size(440, 20);
            this.textBoxMediaStorePath.TabIndex = 1;
            // 
            // labelBlobDirPath
            // 
            this.labelBlobDirPath.AutoSize = true;
            this.labelBlobDirPath.Location = new System.Drawing.Point(5, 20);
            this.labelBlobDirPath.Name = "labelBlobDirPath";
            this.labelBlobDirPath.Size = new System.Drawing.Size(87, 13);
            this.labelBlobDirPath.TabIndex = 0;
            this.labelBlobDirPath.Text = "Mediastore-Path:";
            // 
            // groupBoxMediaStore
            // 
            this.groupBoxMediaStore.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxMediaStore.Controls.Add(this.pictureBoxPort);
            this.groupBoxMediaStore.Controls.Add(this.textBoxPort);
            this.groupBoxMediaStore.Controls.Add(this.labelPort);
            this.groupBoxMediaStore.Controls.Add(this.pictureBoxServer);
            this.groupBoxMediaStore.Controls.Add(this.textBoxServer);
            this.groupBoxMediaStore.Controls.Add(this.labelServer);
            this.groupBoxMediaStore.Controls.Add(this.pictureBoxProtocol);
            this.groupBoxMediaStore.Controls.Add(this.textBoxProtocol);
            this.groupBoxMediaStore.Controls.Add(this.labelProtocol);
            this.groupBoxMediaStore.Location = new System.Drawing.Point(4, 215);
            this.groupBoxMediaStore.Name = "groupBoxMediaStore";
            this.groupBoxMediaStore.Size = new System.Drawing.Size(618, 104);
            this.groupBoxMediaStore.TabIndex = 12;
            this.groupBoxMediaStore.TabStop = false;
            this.groupBoxMediaStore.Text = "MediaStore";
            // 
            // pictureBoxPort
            // 
            this.pictureBoxPort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxPort.Location = new System.Drawing.Point(589, 69);
            this.pictureBoxPort.Name = "pictureBoxPort";
            this.pictureBoxPort.Size = new System.Drawing.Size(25, 20);
            this.pictureBoxPort.TabIndex = 8;
            this.pictureBoxPort.TabStop = false;
            // 
            // textBoxPort
            // 
            this.textBoxPort.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxPort.Location = new System.Drawing.Point(106, 69);
            this.textBoxPort.Name = "textBoxPort";
            this.textBoxPort.PasswordChar = '*';
            this.textBoxPort.Size = new System.Drawing.Size(477, 20);
            this.textBoxPort.TabIndex = 7;
            // 
            // labelPort
            // 
            this.labelPort.AutoSize = true;
            this.labelPort.Location = new System.Drawing.Point(5, 72);
            this.labelPort.Name = "labelPort";
            this.labelPort.Size = new System.Drawing.Size(29, 13);
            this.labelPort.TabIndex = 6;
            this.labelPort.Text = "Port:";
            // 
            // pictureBoxServer
            // 
            this.pictureBoxServer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxServer.Location = new System.Drawing.Point(589, 43);
            this.pictureBoxServer.Name = "pictureBoxServer";
            this.pictureBoxServer.Size = new System.Drawing.Size(25, 20);
            this.pictureBoxServer.TabIndex = 5;
            this.pictureBoxServer.TabStop = false;
            // 
            // textBoxServer
            // 
            this.textBoxServer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxServer.Location = new System.Drawing.Point(106, 43);
            this.textBoxServer.Name = "textBoxServer";
            this.textBoxServer.Size = new System.Drawing.Size(477, 20);
            this.textBoxServer.TabIndex = 4;
            // 
            // labelServer
            // 
            this.labelServer.AutoSize = true;
            this.labelServer.Location = new System.Drawing.Point(5, 46);
            this.labelServer.Name = "labelServer";
            this.labelServer.Size = new System.Drawing.Size(41, 13);
            this.labelServer.TabIndex = 3;
            this.labelServer.Text = "Server:";
            // 
            // pictureBoxProtocol
            // 
            this.pictureBoxProtocol.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxProtocol.Location = new System.Drawing.Point(589, 17);
            this.pictureBoxProtocol.Name = "pictureBoxProtocol";
            this.pictureBoxProtocol.Size = new System.Drawing.Size(25, 20);
            this.pictureBoxProtocol.TabIndex = 2;
            this.pictureBoxProtocol.TabStop = false;
            // 
            // textBoxProtocol
            // 
            this.textBoxProtocol.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxProtocol.Location = new System.Drawing.Point(106, 17);
            this.textBoxProtocol.Name = "textBoxProtocol";
            this.textBoxProtocol.Size = new System.Drawing.Size(477, 20);
            this.textBoxProtocol.TabIndex = 1;
            // 
            // labelProtocol
            // 
            this.labelProtocol.AutoSize = true;
            this.labelProtocol.Location = new System.Drawing.Point(5, 20);
            this.labelProtocol.Name = "labelProtocol";
            this.labelProtocol.Size = new System.Drawing.Size(49, 13);
            this.labelProtocol.TabIndex = 0;
            this.labelProtocol.Text = "Protocol:";
            // 
            // InitOModules
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(623, 563);
            this.Controls.Add(this.groupBoxMediaStore);
            this.Controls.Add(this.groupBoxFilesystem);
            this.Controls.Add(this.radGridViewOutput);
            this.Controls.Add(this.groupBoxSecurity);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Name = "InitOModules";
            this.Text = "Init";
            this.groupBoxSecurity.ResumeLayout(false);
            this.groupBoxSecurity.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewOutput.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewOutput)).EndInit();
            this.groupBoxFilesystem.ResumeLayout(false);
            this.groupBoxFilesystem.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBoxMediaStore.ResumeLayout(false);
            this.groupBoxMediaStore.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxServer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxProtocol)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.GroupBox groupBoxSecurity;
        private System.Windows.Forms.PictureBox pictureBoxUserName;
        private System.Windows.Forms.TextBox textBoxUser;
        private System.Windows.Forms.Label labelUser;
        private System.Windows.Forms.PictureBox pictureBoxPassword;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.PictureBox pictureBoxGroup;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label labelGroup;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label labelEmailAddress;
        private Telerik.WinControls.UI.RadGridView radGridViewOutput;
        private System.Windows.Forms.GroupBox groupBoxFilesystem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label labelWatcherPath;
        private System.Windows.Forms.Button buttonAddFolder;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox textBoxMediaStorePath;
        private System.Windows.Forms.Label labelBlobDirPath;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.GroupBox groupBoxMediaStore;
        private System.Windows.Forms.PictureBox pictureBoxPort;
        private System.Windows.Forms.TextBox textBoxPort;
        private System.Windows.Forms.Label labelPort;
        private System.Windows.Forms.PictureBox pictureBoxServer;
        private System.Windows.Forms.TextBox textBoxServer;
        private System.Windows.Forms.Label labelServer;
        private System.Windows.Forms.PictureBox pictureBoxProtocol;
        private System.Windows.Forms.TextBox textBoxProtocol;
        private System.Windows.Forms.Label labelProtocol;
    }
}