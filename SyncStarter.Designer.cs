﻿namespace SyncStarterModule
{
    partial class SyncStarter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn1 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor1 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.splitContainer = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel3 = new Telerik.WinControls.UI.SplitPanel();
            this.radGridViewModuleList = new Telerik.WinControls.UI.RadGridView();
            this.splitPanel4 = new Telerik.WinControls.UI.SplitPanel();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.radGridViewOutput = new Telerik.WinControls.UI.RadGridView();
            this.outputGridViewItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).BeginInit();
            this.splitPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewModuleList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewModuleList.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewOutput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewOutput.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.outputGridViewItemBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(1280, 479);
            this.radSplitContainer1.TabIndex = 0;
            this.radSplitContainer1.TabStop = false;
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.splitContainer);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(1280, 204);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.07052632F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -41);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // splitContainer
            // 
            this.splitContainer.Controls.Add(this.splitPanel3);
            this.splitContainer.Controls.Add(this.splitPanel4);
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            // 
            // 
            // 
            this.splitContainer.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitContainer.Size = new System.Drawing.Size(1280, 204);
            this.splitContainer.TabIndex = 1;
            this.splitContainer.TabStop = false;
            // 
            // splitPanel3
            // 
            this.splitPanel3.Controls.Add(this.radGridViewModuleList);
            this.splitPanel3.Location = new System.Drawing.Point(0, 0);
            this.splitPanel3.Name = "splitPanel3";
            // 
            // 
            // 
            this.splitPanel3.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel3.Size = new System.Drawing.Size(325, 204);
            this.splitPanel3.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(-0.2449749F, 0F);
            this.splitPanel3.SizeInfo.SplitterCorrection = new System.Drawing.Size(-195, 0);
            this.splitPanel3.TabIndex = 0;
            this.splitPanel3.TabStop = false;
            this.splitPanel3.Text = "splitPanel3";
            // 
            // radGridViewModuleList
            // 
            this.radGridViewModuleList.BackColor = System.Drawing.SystemColors.Control;
            this.radGridViewModuleList.Cursor = System.Windows.Forms.Cursors.Default;
            this.radGridViewModuleList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridViewModuleList.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.radGridViewModuleList.ForeColor = System.Drawing.Color.Black;
            this.radGridViewModuleList.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.radGridViewModuleList.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.radGridViewModuleList.MasterTemplate.AllowAddNewRow = false;
            this.radGridViewModuleList.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewTextBoxColumn1.EnableExpressionEditor = false;
            gridViewTextBoxColumn1.FieldName = "Name";
            gridViewTextBoxColumn1.HeaderText = "Module";
            gridViewTextBoxColumn1.Name = "Module";
            gridViewTextBoxColumn1.Width = 153;
            gridViewTextBoxColumn2.EnableExpressionEditor = false;
            gridViewTextBoxColumn2.FieldName = "Function";
            gridViewTextBoxColumn2.HeaderText = "Function";
            gridViewTextBoxColumn2.Name = "Function";
            gridViewTextBoxColumn2.Width = 152;
            this.radGridViewModuleList.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.radGridViewModuleList.MasterTemplate.EnableFiltering = true;
            this.radGridViewModuleList.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridViewModuleList.Name = "radGridViewModuleList";
            this.radGridViewModuleList.ReadOnly = true;
            this.radGridViewModuleList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radGridViewModuleList.Size = new System.Drawing.Size(325, 204);
            this.radGridViewModuleList.TabIndex = 0;
            // 
            // splitPanel4
            // 
            this.splitPanel4.AutoScroll = true;
            this.splitPanel4.Location = new System.Drawing.Point(329, 0);
            this.splitPanel4.Name = "splitPanel4";
            // 
            // 
            // 
            this.splitPanel4.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel4.Size = new System.Drawing.Size(951, 204);
            this.splitPanel4.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.2449749F, 0F);
            this.splitPanel4.SizeInfo.SplitterCorrection = new System.Drawing.Size(195, 0);
            this.splitPanel4.TabIndex = 1;
            this.splitPanel4.TabStop = false;
            this.splitPanel4.Text = "splitPanel4";
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.radGridViewOutput);
            this.splitPanel2.Location = new System.Drawing.Point(0, 208);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(1280, 271);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.07052632F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 41);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // radGridViewOutput
            // 
            this.radGridViewOutput.BackColor = System.Drawing.SystemColors.Control;
            this.radGridViewOutput.Cursor = System.Windows.Forms.Cursors.Default;
            this.radGridViewOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridViewOutput.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.radGridViewOutput.ForeColor = System.Drawing.Color.Black;
            this.radGridViewOutput.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.radGridViewOutput.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.radGridViewOutput.MasterTemplate.AllowAddNewRow = false;
            this.radGridViewOutput.MasterTemplate.AllowDeleteRow = false;
            this.radGridViewOutput.MasterTemplate.AllowEditRow = false;
            this.radGridViewOutput.MasterTemplate.AutoGenerateColumns = false;
            this.radGridViewOutput.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewDateTimeColumn1.EnableExpressionEditor = false;
            gridViewDateTimeColumn1.FieldName = "Stamp";
            gridViewDateTimeColumn1.HeaderText = "Stamp";
            gridViewDateTimeColumn1.IsAutoGenerated = true;
            gridViewDateTimeColumn1.Name = "Stamp";
            gridViewDateTimeColumn1.SortOrder = Telerik.WinControls.UI.RadSortOrder.Descending;
            gridViewDateTimeColumn1.Width = 172;
            gridViewTextBoxColumn3.EnableExpressionEditor = false;
            gridViewTextBoxColumn3.FieldName = "Type";
            gridViewTextBoxColumn3.HeaderText = "Type";
            gridViewTextBoxColumn3.IsAutoGenerated = true;
            gridViewTextBoxColumn3.Name = "Type";
            gridViewTextBoxColumn3.Width = 172;
            gridViewTextBoxColumn4.EnableExpressionEditor = false;
            gridViewTextBoxColumn4.FieldName = "Module";
            gridViewTextBoxColumn4.HeaderText = "Module";
            gridViewTextBoxColumn4.IsAutoGenerated = true;
            gridViewTextBoxColumn4.Name = "Module";
            gridViewTextBoxColumn4.Width = 183;
            gridViewTextBoxColumn5.EnableExpressionEditor = false;
            gridViewTextBoxColumn5.FieldName = "Function";
            gridViewTextBoxColumn5.HeaderText = "Function";
            gridViewTextBoxColumn5.IsAutoGenerated = true;
            gridViewTextBoxColumn5.Name = "Function";
            gridViewTextBoxColumn5.Width = 188;
            gridViewTextBoxColumn6.EnableExpressionEditor = false;
            gridViewTextBoxColumn6.FieldName = "Message";
            gridViewTextBoxColumn6.HeaderText = "Message";
            gridViewTextBoxColumn6.IsAutoGenerated = true;
            gridViewTextBoxColumn6.Name = "Message";
            gridViewTextBoxColumn6.Width = 548;
            this.radGridViewOutput.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewDateTimeColumn1,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6});
            this.radGridViewOutput.MasterTemplate.DataSource = this.outputGridViewItemBindingSource;
            this.radGridViewOutput.MasterTemplate.EnableFiltering = true;
            this.radGridViewOutput.MasterTemplate.SelectionMode = Telerik.WinControls.UI.GridViewSelectionMode.CellSelect;
            sortDescriptor1.Direction = System.ComponentModel.ListSortDirection.Descending;
            sortDescriptor1.PropertyName = "Stamp";
            this.radGridViewOutput.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor1});
            this.radGridViewOutput.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.radGridViewOutput.Name = "radGridViewOutput";
            this.radGridViewOutput.ReadOnly = true;
            this.radGridViewOutput.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radGridViewOutput.Size = new System.Drawing.Size(1280, 271);
            this.radGridViewOutput.TabIndex = 3;
            // 
            // outputGridViewItemBindingSource
            // 
            this.outputGridViewItemBindingSource.DataSource = typeof(AutomationLibrary.Models.OutputGridViewItem);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // SyncStarter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1280, 479);
            this.Controls.Add(this.radSplitContainer1);
            this.Name = "SyncStarter";
            this.Text = "SyncStarter";
            this.Load += new System.EventHandler(this.SyncStarter_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).EndInit();
            this.splitPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewModuleList.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewModuleList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewOutput.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewOutput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.outputGridViewItemBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.RadSplitContainer splitContainer;
        private Telerik.WinControls.UI.SplitPanel splitPanel3;
        private Telerik.WinControls.UI.SplitPanel splitPanel4;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.RadGridView radGridViewOutput;
        private System.Windows.Forms.BindingSource outputGridViewItemBindingSource;
        private Telerik.WinControls.UI.RadGridView radGridViewModuleList;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
    }
}