﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncStarterModule.Primitives
{
    public enum Module
    {
        ActiveDirectorySyncModule,
        AssemblyImporter,
        BillModule,
        CommandLineRunModule,
        CSVManipulation,
        CSVToSQLScript,
        DevelopmentModule,
        FacebookConnectorModule,
        FileManagementModule,
        GeminiConnectorModule,
        GenerateOntologyAssembly,
        GitConnectorModule,
        GoogleCalendarConnector,
        HtmlEditorModule,
        ImportExport_Module,
        JiraConnectorModule,
        LocalizationModule,
        MailModule,
        MaintainenceModule,
        MediaViewerModule,
        OntologyItemsModule,
        OModules,
        OutlookMailConnector,
        ProcessModule,
        PsScriptOutputParserModule,
        ReportModule,
        SQLImporter,
        TestAutomationModule,
        TextParserModule,
        TFSConnectorModule,
        TypedTaggingConnector,
        WrikeConnectorModule
    }

    class ModulePrimitives
    {
    }
}
