﻿using AutomationLibrary.Controller;
using AutomationLibrary.Models;
using GenerateOntologyAssembly;
using GenerateOntologyAssembly.Models;
using GitConnectorModule;
using GitConnectorModule.Models;
using OModules.Services;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using SyncStarterModule.Controller;
using SyncStarterModule.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TFSConnectorModule;
using TFSConnectorModule.Models;
using WrikeConnectorModule;
using WrikeConnectorModule.Models;

namespace SyncStarterModule
{
    class Program
    {
        private static Globals globals = new Globals();
        [STAThread]
        static void Main(string[] args)
        {
            ProtectConfig();
            Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
            var commandLine = new CommandLine(args, globals);

            if (string.IsNullOrEmpty(commandLine.Module))
            {
                var form = new SyncStarter();
                form.ShowDialog();
            }

            if (!commandLine.IsValid)
            {
                Console.WriteLine(commandLine.ToString());
                Environment.Exit(-1);
            }
           
            var types = Assembly.GetExecutingAssembly().GetTypes().ToList();
            types.AddRange(Assembly.GetExecutingAssembly().GetReferencedAssemblies().Where(ass => ass.Name == "AutomationLibrary").Select(ass => Assembly.Load(ass)).SelectMany(ass => ass.GetTypes()));
            var isDone = false;
            foreach (var typeItem in types)
            {
                if (typeItem.BaseType == typeof(BaseController))
                {
                    ConstructorInfo ctor = typeItem.GetConstructor(new[] { typeof(CommandLine), typeof(Globals) });
                    if (ctor == null) continue;

                    var controller = (BaseController)Activator.CreateInstance(typeItem);

                    if (controller.IsResponsible(commandLine.Module))
                    {
                        controller = (BaseController)ctor.Invoke(new object[] { commandLine, globals });
                        commandLine.AddUsage(controller.Syntax);
                        if (!controller.IsValid && !isDone)
                        {
                            Console.WriteLine(commandLine.ToString());
                            Console.WriteLine(controller.ToString());
                            Environment.Exit(-1);
                        }

                        var workResult = controller.DoWork();

                        if (workResult.GUID == globals.LState_Error.GUID)
                        {
                            commandLine.ErrorMessage = workResult.Additional1;
                            Console.WriteLine(commandLine.ToString());
                            Console.WriteLine(controller.ToString());
                            Environment.Exit(-1);
                        }

                        Console.WriteLine(workResult.Additional1);
                        isDone = true;
                    }

                }


            }

            if (!isDone)
            {
                commandLine.ErrorMessage = "The Module is not present!";
                commandLine.IsValid = false;
                Console.WriteLine(commandLine.ToString());
                Environment.Exit(-1);
            }
            
        }

        private static void ProtectConfig()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            ConfigurationSection section = config.GetSection("Authentication");
            if (section != null)
            {
                if (!section.SectionInformation.IsProtected)
                {
                    if (!section.ElementInformation.IsLocked)
                    {
                        section.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
                        section.SectionInformation.ForceSave = true;
                        config.Save(ConfigurationSaveMode.Full);
                    }
                }
            }
        }

    }
}
