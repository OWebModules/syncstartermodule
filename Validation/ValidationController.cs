﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using SyncStarterModule.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncStarterModule.Validation
{
    public static class ValidationController
    {
        public static clsOntologyItem ValidateExtractPasswordRequest(ExtractPasswordRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdUserName))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdUsername of Masteruser is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdUserName))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdUsername of Masteruser is no valid GUID!";
                return result;
            }

            if (string.IsNullOrEmpty(request.Password))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "Password is empty!";
                return result;
            }

            return result;
        }
    }
}
