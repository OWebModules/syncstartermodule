﻿using OModules.Services;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Logging;
using SyncStarterModule.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationLibrary.Controller;

namespace SyncStarterModule.Controller
{
    public enum OModulesFunction
    {
        SyncMethods
    }
    public class OModulesController : BaseController, IModuleController
    {

        public OModulesFunction Function { get; private set; }

        public SyncOModuleFunctionsParam SyncOModuleFunctionsParameters { get; private set; }
        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(OModules).ToLower();
        }

        public override string Syntax
        {
            get
            {
                return $"{nameof(OModules)} {nameof(Function)}:{nameof(OModulesFunction.SyncMethods)}";
            }
        }

        public clsOntologyItem Module => AutomationLibrary.Modules.Config.LocalData.Object_OModules;

        public List<Type> Functions => new List<Type> { typeof(SyncOModuleFunctionsParam) };

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var syncConnector = new OModuleSync(globals);

            switch(Function)
            {
                case OModulesFunction.SyncMethods:
                    try
                    {
                        var syncTask = await syncConnector.SyncOModules(SyncOModuleFunctionsParameters.MessageOutput);

                        result = syncTask.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Synced {syncTask.Result.CountControllers} Controllers\nSynced {syncTask.Result.CountActions} Actions\nSynced {syncTask.Result.CountParameters} Parameters";


                        }
                        return result;
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;
                        return result;

                    }
            }

            return result;
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var syncConnector = new OModuleSync(globals);

            switch (Function)
            {
                case OModulesFunction.SyncMethods:
                    try
                    {
                        var syncTask = syncConnector.SyncOModules(SyncOModuleFunctionsParameters.MessageOutput);
                        syncTask.Wait();

                        result = syncTask.Result.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Synced {syncTask.Result.Result.CountControllers} Controllers\nSynced {syncTask.Result.Result.CountActions} Actions\nSynced {syncTask.Result.Result.CountParameters} Parameters";


                        }
                        return result;
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;
                        return result;

                    }
            }

            return result;
        }

        public OModulesController(SyncOModuleFunctionsParam syncParam, Globals globals) : base(globals)
        {
            SyncOModuleFunctionsParameters = syncParam;
            IsValid = true;
            var validationResult = SyncOModuleFunctionsParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public OModulesController(CommandLine commandLine, Globals globals) : base(globals, commandLine)
        {
            IsValid = true;
            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Function).ToLower());

            if (key == null)
            {
                IsValid = false;
                ErrorMessage += "You have to provide an Function\n";
            }
            else
            {
                if (commandLine.AdditionalParameters[key].ToLower() == OModulesFunction.SyncMethods.ToString().ToLower())
                {
                    Function = OModulesFunction.SyncMethods;
                }
                else
                {
                    IsValid = false;
                    ErrorMessage += "You have to provide a valid Function\n";
                }
            }

            switch (Function)
            {
                case OModulesFunction.SyncMethods:
                    SyncOModuleFunctionsParameters = new SyncOModuleFunctionsParam
                    {
                        MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)

                    };
                    var validateResultAddVariables = SyncOModuleFunctionsParameters.ValidateParam(commandLine, globals);
                    IsValid = (validateResultAddVariables.GUID == globals.LState_Success.GUID);
                    ErrorMessage += validateResultAddVariables.Additional1;
                    break;

            }

        }

        public OModulesController() { }
    }


}