﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using SecurityModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncStarterModule.Controller
{
    public class InitOmodulesController : NotifyPropertyChange
    {

        private Globals globals;

        private bool isUserSet;
        public bool IsUserSet
        {
            get
            {
                return isUserSet;
            }
            set
            {
                isUserSet = value;
                RaisePropertyChanged(nameof(IsUserSet));   
            }
        }

        private bool isGroupSet;
        public bool IsGroupSet
        {
            get
            {
                return isGroupSet;
            }
            set
            {
                isGroupSet = value;
                RaisePropertyChanged(nameof(IsGroupSet));
            }
        }

        private bool isPasswordSet;
        public bool IsPasswordSet
        {
            get
            {
                return isPasswordSet;
            }
            set
            {
                isPasswordSet = value;
                RaisePropertyChanged(nameof(IsPasswordSet));
            }
        }

        private bool isEmailAddressSet;
        public bool IsEmailAddressSet
        {
            get
            {
                return isEmailAddressSet;
            }
            set
            {
                isEmailAddressSet = value;
                RaisePropertyChanged(nameof(IsPasswordSet));
            }
        }

        private bool isMediaStorePathSet;
        public bool IsMediaStorePathSet
        {
            get
            {
                return isMediaStorePathSet;
            }
            set
            {
                isMediaStorePathSet = value;
                RaisePropertyChanged(nameof(IsMediaStorePathSet));
            }
        }

        private bool isWatcherPathSet;
        public bool IsWatcherPathSet
        {
            get
            {
                return isWatcherPathSet;
            }
            set
            {
                isWatcherPathSet = value;
                RaisePropertyChanged(nameof(IsWatcherPathSet));
            }
        }



        public async Task CheckBaseData()
        {
            await Task.Run(() =>
            {
                var securityController = new SecurityController(globals);

                
            });
        }


        public InitOmodulesController(Globals globals)
        {
            this.globals = globals;
        }
    }
}
