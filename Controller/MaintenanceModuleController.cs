﻿using ImportExport_Module;
using MaintainenceModule.Models;
using OModules.Controllers;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Models;
using OntoMsg_Module.Logging;
using SyncStarterModule.Attributes;
using SyncStarterModule.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using AutomationLibrary.Controller;
using System.ComponentModel;
using SyncStarterModule.Model;

namespace SyncStarterModule.Controller
{
    public enum MaintenanceFunction
    {
        CleanupNameItems = 0,
        MoveClasses = 1,
        RelateSimilar = 2,
        CleanupWebPosts = 3,
        MoveObjects = 4,
        DeleteUnrelatedObjects = 5,
        RepairAttributesAndRelations = 6
    }
    public class MaintenanceModuleController : BaseController, IModuleController
    {
        public CleanupMultipleItemsByNameRequest CleanupMultipleItemsByNameRequest { get; private set; }
        public RepairAttributeAndRelationsParam RepairAttributeAndRelationsParameters { get; private set; }
        public ObjectCreateType SearchType { get; private set; }
        public List<clsOntologyItem> ObjectsForRelation { get; private set; } = new List<clsOntologyItem>();
        public clsObjectRel BaseRelation { get; private set; }

        public CleanupWebPostsParam CleanupWebPostsParameters { get; private set; }

        public MaintenanceFunction Function { get; private set; }
        public List<clsOntologyItem> ClassesToMove { get; private set; }

        public MoveObjectListsParam MoveObjectsParameter { get; private set; }

        public DeleteUnrelatedObjectsParam DeleteUnrelatedObjectsParameters { get; private set; }

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(MaintainenceModule).ToLower();
        }

        public override string Syntax
        {
            get
            {
                var sbSyntax = new StringBuilder();
                sbSyntax.AppendLine($"{nameof(MaintainenceModule)} {nameof(Function)}:{nameof(MaintenanceFunction.CleanupNameItems)} {nameof(MaintainenceModule.Models.CleanupMultipleItemsByNameRequest.IdClassSource)}:<id of source-class> {nameof(MaintainenceModule.Models.CleanupMultipleItemsByNameRequest.IdClassSource)}:<id of dest-class> {nameof(MaintainenceModule.Models.CleanupMultipleItemsByNameRequest.MergeAttributes)}:<true|false> {nameof(MaintainenceModule.Models.CleanupMultipleItemsByNameRequest.MergeRelations)}:<true|false>");
                sbSyntax.AppendLine($"{nameof(MaintainenceModule)} {nameof(Function)}:{nameof(MaintenanceFunction.MoveClasses)} {nameof(ClassesToMove)}:<Json-List of Classes, like [{{GÚID:<Id of class>, GUID_Parent:<Id of new parent-class>}}]>");
                sbSyntax.AppendLine($"{nameof(MaintainenceModule)} {nameof(Function)}:{nameof(MaintenanceFunction.RelateSimilar)} {nameof(SearchType)}:[{nameof(ObjectCreateType.Contains)}|{nameof(ObjectCreateType.ContainsCreate)}|{nameof(ObjectCreateType.ContainsUnique)}|{nameof(ObjectCreateType.ContainsUniqueCreate)}|" +
                        $"{nameof(ObjectCreateType.SameName)}|{nameof(ObjectCreateType.SameNameCreate)}|{nameof(ObjectCreateType.SameNameUnique)}|{nameof(ObjectCreateType.SameNameUniqueCreate)}] {nameof(ObjectsForRelation)}:<Json-Item of Lists to check for relating like [{{GUID:<GUID of object>,Name:<Name of Object>,GUID_Parent:<GUID of class of object>}},{{..}}] {nameof(BaseRelation)}:<Json-Item of BaseRelation, like [{{ID_Object:<Id of the left object>, ID_Parent_Other:<Id of right class>,ID_RelationType:<Id of relationtype>}}]>");
                sbSyntax.AppendLine($"{nameof(MaintainenceModule)} {nameof(Function)}:{nameof(MaintenanceFunction.MoveObjects)} {nameof(MoveObjectListsParam.IdConfig)}:<Config-id>");
                sbSyntax.AppendLine($"{nameof(MaintainenceModule)} {nameof(Function)}:{nameof(MaintenanceFunction.DeleteUnrelatedObjects)} {nameof(DeleteUnrelatedObjectsParam.IdConfig)}:<Config-id>");
                sbSyntax.AppendLine($"{nameof(MaintainenceModule)} {nameof(Function)}:{nameof(MaintenanceFunction.RepairAttributesAndRelations)} {nameof(RepairAttributeAndRelationsParam.IdConfig)}:<Config-id>");
                return sbSyntax.ToString();
            }
        }

        public clsOntologyItem Module => AutomationLibrary.Modules.Config.LocalData.Object_MaintainenceModule;

        public List<Type> Functions => new List<Type> { typeof(CleanupWebPostsParam), typeof(MoveObjectListsParam), typeof(DeleteUnrelatedObjectsParam), typeof(RepairAttributeAndRelationsParam) };

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var syncConnector = new MaintainenceModule.MaintenenceController(globals);
            var result = globals.LState_Success.Clone();
            try
            {
                switch (Function)
                {
                    case MaintenanceFunction.CleanupWebPosts:

                        var request = new CleanupWebPostsRequest
                        {
                            MessageOutput = CleanupWebPostsParameters.MessageOutput
                        };
                        var taskResult = await syncConnector.CleanupWebPosts(request);

                        result = taskResult.ResultState;

                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Cleanup items";
                        }
                        break;
                    case MaintenanceFunction.MoveObjects:

                        var moveObjectsController = new OntologyItemsModule.ObjectEditController(globals);
                        var requestMoveObjects = new MoveObjectListRequest()
                        {
                            MessageOutput = MoveObjectsParameter.MessageOutput,
                            IdConfig = MoveObjectsParameter.IdConfig
                        };
                        var moveObjectsResult = await moveObjectsController.MoveObjectList(requestMoveObjects);

                        result = moveObjectsResult.ResultState;

                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Cleanup items";
                        }
                        break;
                    case MaintenanceFunction.DeleteUnrelatedObjects:

                        var maintenanceController = new MaintainenceModule.MaintenenceController(globals);
                        var deleteUnrelatedObjectsRequest = new DeleteUnrelatedObjectsRequest(DeleteUnrelatedObjectsParameters.IdConfig)
                        {
                            MessageOutput = MoveObjectsParameter.MessageOutput
                        };
                        var deleteUnrelatedObjectsResult = await maintenanceController.DeleteUnrelatedObjects(deleteUnrelatedObjectsRequest);

                        result = deleteUnrelatedObjectsResult.ResultState;

                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Deleted {deleteUnrelatedObjectsResult.Result.DeletedObjects.Count} objects.";
                        }
                        break;
                    case MaintenanceFunction.RepairAttributesAndRelations:

                        var maintenanceController1 = new MaintainenceModule.MaintenenceController(globals);
                        var repairAttributesAndRelations = new RepairAttributesAndRelationsRequest(RepairAttributeAndRelationsParameters.IdConfig, RepairAttributeAndRelationsParameters.CancellationTokenSource.Token)
                        {
                            MessageOutput = RepairAttributeAndRelationsParameters.MessageOutput
                        };
                        var repairResult = await maintenanceController1.RepairAttributesAndRelations(repairAttributesAndRelations);

                        result = repairResult.ResultState;

                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"";
                        }
                        break;
                    default:
                        result = globals.LState_Error.Clone();
                        result.Additional1 = "No valid function provided!";
                        break;
                }
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"Error while executing Function: {ex.Message}";
                return result;
                
            }
            return result;
        }

        public async Task<clsOntologyItem> ExtractPasswords(ExtractPasswordRequest request)
        {
            var taskResult = await Task.Run<clsOntologyItem>(async() =>
            {
                var result = globals.LState_Success.Clone();

                request.MessageOutput?.OutputInfo("Validate request...");
                result = Validation.ValidationController.ValidateExtractPasswordRequest(request, globals);
                if (result.GUID == globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Validated request.");

                var serviceAgent = new SyncStarterModule.Services.ElasticServiceAgent(globals);

                request.MessageOutput?.OutputInfo("Get configurations...");
                var getModuleMetasResult = serviceAgent.GetModuleMetas();

                result = getModuleMetasResult.ResultState;

                if (result.GUID == globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo($"Have {getModuleMetasResult.Result.ModuleMetas.Count} Configurations.");

                request.MessageOutput?.OutputInfo("Get automation-types...");
                var types = AppDomain.CurrentDomain.GetAssemblies().Where(ass => ass.FullName.Contains("AutomationLibrary") || ass.FullName.Contains("SyncStarterModule")).SelectMany(ass => ass.GetTypes()).ToList();
                var securityController = new SecurityModule.SecurityController(globals);

                request.MessageOutput?.OutputInfo("Login...");
                var loginResult = await securityController.LoginWithUser(request.IdUserName, request.Password);
                result = loginResult.Result;

                if (result.GUID == globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Loged in.");

                var relationConfig = new clsRelationConfig(globals);

                request.MessageOutput?.OutputInfo($"Have {types.Count} automation-types.");
                foreach (var moduleMeta in getModuleMetasResult.Result.ModuleMetas)
                {
                    var type = types.FirstOrDefault(typ => typ.FullName == moduleMeta.ParameterNamespace.Val_String);
                    var passwordProperties = type.GetProperties().Where(prop => prop.GetCustomAttributes().FirstOrDefault(att => att is PasswordPropertyTextAttribute) != null);
                    if (passwordProperties.Any())
                    {
                        var paramInstance = Newtonsoft.Json.JsonConvert.DeserializeObject(moduleMeta.JsonAttribute.Val_String, type);
                        var passwords = passwordProperties.Select(prop => new { prop, value = (string)prop.GetValue(paramInstance) }).Where(propVal => propVal.value != null).ToList();
                        if (passwords.Any())
                        {
                            var userAuthenticationsAndPasswords = (from passw in passwords
                                                                   join auth in moduleMeta.UserAuthentications on passw.prop.Name equals auth.Name into auths
                                                                   from auth in auths.DefaultIfEmpty()
                                                                   select new { passw, auth }).ToList();
                            var userAuthenticationsUsersAndPasswords = (from authPass in userAuthenticationsAndPasswords.Where(authP => authP.auth != null)
                                                                        join user in moduleMeta.UserAuthenticationsToUsers on authPass.auth.GUID equals user.ID_Object
                                                                        select new
                                                                        {
                                                                            authPass,
                                                                            User = new clsOntologyItem
                                                                            {
                                                                                GUID = user.ID_Other,
                                                                                Name = user.Name_Other,
                                                                                GUID_Parent = user.ID_Parent_Other,
                                                                                Type = user.Ontology
                                                                            }
                                                                        }).ToList();

                            var objectsToSave = new List<clsOntologyItem>();
                            var relationsToSave = new List<clsObjectRel>();

                            if (userAuthenticationsUsersAndPasswords.Any())
                            {
                                foreach (var userAuthAndPassword in userAuthenticationsUsersAndPasswords)
                                {

                                    if (userAuthAndPassword.authPass.auth != null && userAuthAndPassword.User != null)
                                    {
                                        var passwordRel = await securityController.GetPassword(userAuthAndPassword.User, request.Password);
                                        result = passwordRel.Result;
                                        if (result.GUID == globals.LState_Error.GUID)
                                        {
                                            request.MessageOutput?.OutputError(result.Additional1);
                                            return result;
                                        }
                                    }
                                    else
                                    {

                                    }
                                }
                            }
                            else
                            {
                                foreach (var passW in passwords)
                                {
                                    var userAuthentication = new clsOntologyItem
                                    {
                                        GUID = globals.NewGUID,
                                        Name = passW.prop.Name,
                                        GUID_Parent = Config.LocalData.Class_User_Authentication.GUID,
                                        Type = globals.Type_Object
                                    };

                                    objectsToSave.Add(userAuthentication);

                                    var user = new clsOntologyItem
                                    {
                                        GUID = globals.NewGUID,
                                        Name = passW.prop.Name,
                                        GUID_Parent = Config.LocalData.Class_user.GUID,
                                        Type = globals.Type_Object
                                    };

                                    objectsToSave.Add(user);

                                    var saveResult = await serviceAgent.SaveObjects(objectsToSave);
                                    result = saveResult;
                                    if (result.GUID == globals.LState_Error.GUID)
                                    {
                                        result.Additional1 = "Error while saving Userauthentification and User!";
                                        request.MessageOutput?.OutputError(result.Additional1);
                                        return result;
                                    }

                                    var passwordResult = await securityController.SavePassword(user, passW.value);
                                    result = passwordResult.ResultState;
                                    if (result.GUID == globals.LState_Error.GUID)
                                    {
                                        result.Additional1 = $"Error while saving the password for {passW.prop.Name}";
                                        request.MessageOutput?.OutputError(result.Additional1);
                                        return result;
                                    };
                                    var passwordItem = passwordResult.Result.GetPasswordItem();

                                    relationsToSave.Add(relationConfig.Rel_ObjectRelation(moduleMeta.ModuleConfig, userAuthentication, Config.LocalData.RelationType_secured_by));
                                    relationsToSave.Add(relationConfig.Rel_ObjectRelation(userAuthentication, user, Config.LocalData.RelationType_secured_by));
                                    relationsToSave.Add(relationConfig.Rel_ObjectRelation(userAuthentication, passwordItem, Config.LocalData.RelationType_secured_by));

                                    saveResult = await serviceAgent.SaveRelations(relationsToSave);
                                    result = saveResult;
                                    if (result.GUID == globals.LState_Error.GUID)
                                    {
                                        result.Additional1 = "Error while saving relations to Userauthentification and User!";
                                        request.MessageOutput?.OutputError(result.Additional1);
                                        return result;
                                    }
                                    passW.prop.SetValue(paramInstance, "");

                                    moduleMeta.JsonAttribute.Val_String = Newtonsoft.Json.JsonConvert.SerializeObject(paramInstance);

                                    var attributesToSave = new List<clsObjectAtt>
                                    {
                                        moduleMeta.JsonAttribute
                                    };

                                    result = await serviceAgent.SaveAttributes(attributesToSave);
                                    if (result.GUID == globals.LState_Error.GUID)
                                    {
                                        result.Additional1 = "Error while saving the new Module-Meta!";
                                        return result;
                                    }

                                }
                            }
                            
                        }
                    }
                }

                return result;
            });

            return taskResult;
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var syncConnector = new MaintainenceModule.MaintenenceController(globals);

            try
            {
                switch (Function)
                {
                    case MaintenanceFunction.CleanupNameItems:
                        var taskResult = syncConnector.CleanupMultipleItemsNames(CleanupMultipleItemsByNameRequest);
                        taskResult.Wait();

                        result = taskResult.Result.ResultState;

                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Deleted {taskResult.Result.Result.CountDeletedItems} items";
                        }
                        break;
                    case MaintenanceFunction.MoveClasses:
                        var syncConnectorOntologyMod = new OntologyModDBConnector(globals);

                        result = syncConnectorOntologyMod.MoveClasses(ClassesToMove);
                        break;
                    case MaintenanceFunction.RelateSimilar:
                        var relateConnector = new OntologyModDBConnector(globals);

                        var request = new RelateSimilarObjectsRequest(SearchType, ObjectsForRelation, BaseRelation);
                        var relateResultTask = relateConnector.RelateSimilarObjects(request);
                        relateResultTask.Wait();
                        result = relateResultTask.Result.ResultState;

                        break;
                    case MaintenanceFunction.CleanupWebPosts:

                        var requestCleanup = new CleanupWebPostsRequest
                        {
                            MessageOutput = CleanupWebPostsParameters.MessageOutput
                        };
                        var taskResultCleanup = syncConnector.CleanupWebPosts(requestCleanup);
                        taskResultCleanup.Wait();

                        result = taskResultCleanup.Result.ResultState;

                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Cleanup items";
                        }
                        break;

                    case MaintenanceFunction.MoveObjects:

                        var moveObjectsController = new OntologyItemsModule.ObjectEditController(globals);
                        var requestMoveObjects = new MoveObjectListRequest()
                        {
                            MessageOutput = CleanupWebPostsParameters.MessageOutput,
                            IdConfig = MoveObjectsParameter.IdConfig
                        };
                        var moveObjectsResult = moveObjectsController.MoveObjectList(requestMoveObjects);
                        moveObjectsResult.Wait();

                        result = moveObjectsResult.Result.ResultState;

                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Cleanup items";
                        }
                        break;
                    case MaintenanceFunction.DeleteUnrelatedObjects:

                        var maintenanceController = new MaintainenceModule.MaintenenceController(globals);
                        var deleteUnrelatedObjectsRequest = new DeleteUnrelatedObjectsRequest(DeleteUnrelatedObjectsParameters.IdConfig)
                        {
                            MessageOutput = MoveObjectsParameter.MessageOutput
                        };
                        var deleteUnrelatedObjectsResult = maintenanceController.DeleteUnrelatedObjects(deleteUnrelatedObjectsRequest);
                        deleteUnrelatedObjectsResult.Wait();

                        result = deleteUnrelatedObjectsResult.Result.ResultState;

                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Deleted {deleteUnrelatedObjectsResult.Result.Result.DeletedObjects.Count} objects.";
                        }
                        break;
                    case MaintenanceFunction.RepairAttributesAndRelations:

                        var maintenanceController1 = new MaintainenceModule.MaintenenceController(globals);
                        var repairAttributesAndRelations = new RepairAttributesAndRelationsRequest(RepairAttributeAndRelationsParameters.IdConfig, RepairAttributeAndRelationsParameters.CancellationTokenSource.Token)
                        {
                            MessageOutput = RepairAttributeAndRelationsParameters.MessageOutput
                        };
                        var repairResult = maintenanceController1.RepairAttributesAndRelations(repairAttributesAndRelations);
                        repairResult.Wait();

                        result = repairResult.Result.ResultState;

                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"";
                        }
                        break;
                    default:
                        result = globals.LState_Error.Clone();
                        result.Additional1 = "No valid function provided!";
                        break;
                }
                
                
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public MaintenanceModuleController(CleanupWebPostsParam param, Globals globals) : base(globals)
        {
            IsValid = true;
            Function = MaintenanceFunction.CleanupWebPosts;
            CleanupWebPostsParameters = param;
            IsValid = true;
            var validationResult = CleanupWebPostsParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }
        }

        public MaintenanceModuleController(MoveObjectListsParam syncParam, Globals globals) : base(globals)
        {
            MoveObjectsParameter = syncParam;
            Function = MaintenanceFunction.MoveObjects;
            IsValid = true;
            var validationResult = MoveObjectsParameter.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public MaintenanceModuleController(DeleteUnrelatedObjectsParam syncParam, Globals globals) : base(globals)
        {
            DeleteUnrelatedObjectsParameters = syncParam;
            Function = MaintenanceFunction.DeleteUnrelatedObjects;
            IsValid = true;
            var validationResult = DeleteUnrelatedObjectsParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public MaintenanceModuleController(RepairAttributeAndRelationsParam syncParam, Globals globals) : base(globals)
        {
            RepairAttributeAndRelationsParameters = syncParam;
            Function = MaintenanceFunction.RepairAttributesAndRelations;
            IsValid = true;
            var validationResult = RepairAttributeAndRelationsParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public MaintenanceModuleController(CommandLine commandLine, Globals globals) : base(globals, commandLine)
        {
            
            
            IsValid = true;
            ErrorMessage = "";

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Function).ToLower());

            if (key == null)
            {
                IsValid = false;
                ErrorMessage += $"You have to provide a valid function\n";
                return;
            }

            

            if (commandLine.AdditionalParameters[key].ToLower() == nameof(MaintenanceFunction.CleanupNameItems).ToLower())
            {
                Function = MaintenanceFunction.CleanupNameItems;
                key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(MaintenanceFunction.CleanupNameItems).ToLower());

                if (key == null)
                {
                    IsValid = false;
                    ErrorMessage += $"You have to provide a valid function: [{MaintenanceFunction.CleanupNameItems}|{MaintenanceFunction.MoveClasses}] \n";
                    return;

                }
                key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(CleanupMultipleItemsByNameRequest.IdClassSource).ToLower());

                if (key == null)
                {
                    IsValid = false;
                    ErrorMessage += "You have to provide a classid for the source-class\n";
                    return;

                }

                var idClassSource = commandLine.AdditionalParameters[key];

                key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(CleanupMultipleItemsByNameRequest.IdClassDest).ToLower());

                if (key == null)
                {
                    IsValid = false;
                    ErrorMessage += "You have to provide a classid for the dest-class\n";
                    return;

                }

                var idClassDest = commandLine.AdditionalParameters[key];

                key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(CleanupMultipleItemsByNameRequest.MergeAttributes).ToLower());

                if (key == null)
                {
                    IsValid = false;
                    ErrorMessage += "You have to provide a true or false for MergeAttributes\n";
                    return;

                }

                var mergeAttributesStr = commandLine.AdditionalParameters[key];

                bool mergeAttributes = false;

                if (!bool.TryParse(mergeAttributesStr, out mergeAttributes))
                {
                    IsValid = false;
                    ErrorMessage += "You have to provide a valid true or false for MergeAttributes\n";
                    return;
                }

                key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(CleanupMultipleItemsByNameRequest.MergeRelations).ToLower());

                if (key == null)
                {
                    IsValid = false;
                    ErrorMessage += "You have to provide a true or false for MergeRelations\n";
                    return;

                }

                var mergeRelationsStr = commandLine.AdditionalParameters[key];

                bool mergeRelations = false;

                if (!bool.TryParse(mergeRelationsStr, out mergeRelations))
                {
                    IsValid = false;
                    ErrorMessage += "You have to provide a valid true or false for MergeRelations\n";
                    return;
                }

                key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(CleanupMultipleItemsByNameRequest.IncludeObjectIds).ToLower());

                var includeObjectIds = new List<string>();

                if (key != null)
                {
                    try
                    {
                        var includeIdlist = commandLine.AdditionalParameters[key];
                        includeObjectIds = includeIdlist.Split(',').Select(includeId => includeId.Trim()).ToList();
                    }
                    catch (Exception)
                    {

                        IsValid = false;
                        ErrorMessage += "No valid list of objectIds to include provided!";
                        return;
                    }


                }

                key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(CleanupMultipleItemsByNameRequest.ExcludeObjectIds).ToLower());

                var excludeObjectIds = new List<string>();

                if (key != null)
                {
                    try
                    {
                        var excludeIdlist = commandLine.AdditionalParameters[key];
                        excludeObjectIds = excludeIdlist.Split(',').Select(excludeId => excludeId.Trim()).ToList();
                    }
                    catch (Exception)
                    {

                        IsValid = false;
                        ErrorMessage += "No valid list of objectIds to exclude provided!";
                        return;
                    }


                }

                CleanupMultipleItemsByNameRequest = new CleanupMultipleItemsByNameRequest(idClassSource, idClassDest)
                {
                    MergeAttributes = mergeAttributes,
                    MergeRelations = mergeRelations,
                    IncludeObjectIds = includeObjectIds,
                    ExcludeObjectIds = excludeObjectIds
                };
            }
            else if (commandLine.AdditionalParameters[key].ToLower() == nameof(MaintenanceFunction.MoveClasses).ToLower())
            {
                Function = MaintenanceFunction.MoveClasses;
                key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(ClassesToMove).ToLower());

                if (key == null)
                {
                    IsValid = false;
                    ErrorMessage += "You have to provide a List of classes to move\n";
                    return;
                }

                var classesToMoveString = commandLine.AdditionalParameters[key];
                try
                {
                    ClassesToMove = Newtonsoft.Json.JsonConvert.DeserializeObject<List<clsOntologyItem>>(classesToMoveString);

                    
                }
                catch (Exception ex)
                {
                    IsValid = false;
                    ErrorMessage += $"The list of classes is no valid JSON: {ex.Message}";
                    return;
                }

               
                
                
            }
            else if (commandLine.AdditionalParameters[key].ToLower() == nameof(MaintenanceFunction.RelateSimilar).ToLower())
            {
                Function = MaintenanceFunction.RelateSimilar;
                key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(SearchType).ToLower());

                if (key == null)
                {
                    IsValid = false;
                    ErrorMessage += $"You have to provide a SearchType [{nameof(ObjectCreateType.Contains)}|{nameof(ObjectCreateType.ContainsCreate)}|{nameof(ObjectCreateType.ContainsUnique)}|{nameof(ObjectCreateType.ContainsUniqueCreate)}|" + 
                        $"{nameof(ObjectCreateType.SameName)}|{nameof(ObjectCreateType.SameNameCreate)}|{nameof(ObjectCreateType.SameNameUnique)}|{nameof(ObjectCreateType.SameNameUniqueCreate)}]\n";
                    return;
                }

                var functionType = commandLine.AdditionalParameters[key];
                try
                {
                    if (functionType == nameof(ObjectCreateType.Contains))
                    {
                        SearchType = ObjectCreateType.Contains;
                    }
                    else if (functionType == nameof(ObjectCreateType.ContainsCreate))
                    {
                        SearchType = ObjectCreateType.ContainsCreate;
                    }
                    else if (functionType == nameof(ObjectCreateType.ContainsUnique))
                    {
                        SearchType = ObjectCreateType.ContainsUnique;
                    }
                    else if (functionType == nameof(ObjectCreateType.ContainsUniqueCreate))
                    {
                        SearchType = ObjectCreateType.ContainsUniqueCreate;
                    }
                    else if (functionType == nameof(ObjectCreateType.SameName))
                    {
                        SearchType = ObjectCreateType.SameName;
                    }
                    else if (functionType == nameof(ObjectCreateType.SameNameCreate))
                    {
                        SearchType = ObjectCreateType.SameNameCreate;
                    }
                    else if (functionType == nameof(ObjectCreateType.SameNameUnique))
                    {
                        SearchType = ObjectCreateType.SameNameUnique;
                    }
                    else if (functionType == nameof(ObjectCreateType.SameNameUniqueCreate))
                    {
                        SearchType = ObjectCreateType.SameNameUniqueCreate;
                    }
                    else
                    {
                        IsValid = false;
                        ErrorMessage += $"You have to provide a SearchType [{nameof(ObjectCreateType.Contains)}|{nameof(ObjectCreateType.ContainsCreate)}|{nameof(ObjectCreateType.ContainsUnique)}|{nameof(ObjectCreateType.ContainsUniqueCreate)}|" +
                            $"{nameof(ObjectCreateType.SameName)}|{nameof(ObjectCreateType.SameNameCreate)}|{nameof(ObjectCreateType.SameNameUnique)}|{nameof(ObjectCreateType.SameNameUniqueCreate)}]\n";
                        return;
                    }

                    //SearchType = Newtonsoft.Json.JsonConvert.DeserializeObject<List<clsOntologyItem>>(classesToMoveString);

                    key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(BaseRelation).ToLower());

                    if (key == null)
                    {
                        IsValid = false;
                        ErrorMessage += $"{nameof(BaseRelation)}:<Json-Item of BaseRelation, like [{{ID_Object:<Id of the left object>, ID_Parent_Other:<Id of right class>,ID_RelationType:<Id of relationtype>}}]\n";
                        return;
                    }

                    BaseRelation = Newtonsoft.Json.JsonConvert.DeserializeObject<clsObjectRel>(commandLine.AdditionalParameters[key]);

                    key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(ObjectsForRelation).ToLower());

                    if (key == null)
                    {
                        IsValid = false;
                        ErrorMessage += $"{nameof(ObjectsForRelation)}:<Json-Item of Lists to check for relating like [{{GUID:<GUID of object>,Name:<Name of Object>,GUID_Parent:<GUID of class of object>}},{{..}}]\n";
                        return;
                    }

                    var objectsForRelations = commandLine.AdditionalParameters[key];

                    ObjectsForRelation = Newtonsoft.Json.JsonConvert.DeserializeObject<List<clsOntologyItem>>(commandLine.AdditionalParameters[key]);

                }
                catch (Exception ex)
                {
                    IsValid = false;
                    ErrorMessage += $"The list of classes is no valid JSON: {ex.Message}";
                    return;
                }




            }
            else if (CommandLine.AdditionalParameters[key].ToLower() == nameof(MaintenanceFunction.CleanupWebPosts).ToLower())
            {
                Function = MaintenanceFunction.CleanupWebPosts;
                CleanupWebPostsParameters = new CleanupWebPostsParam
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };
            }
            else if (CommandLine.AdditionalParameters[key].ToLower() == nameof(MaintenanceFunction.RepairAttributesAndRelations).ToLower())
            {
                Function = MaintenanceFunction.RepairAttributesAndRelations;
                RepairAttributeAndRelationsParameters = new RepairAttributeAndRelationsParam
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };
            }
            else if (commandLine.AdditionalParameters[key].ToLower() == nameof(MaintenanceFunction.MoveObjects).ToLower())
            {
                Function = MaintenanceFunction.MoveObjects;
                MoveObjectsParameter = new MoveObjectListsParam
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };
            }
            else if (commandLine.AdditionalParameters[key].ToLower() == nameof(MaintenanceFunction.DeleteUnrelatedObjects).ToLower())
            {
                Function = MaintenanceFunction.DeleteUnrelatedObjects;
                DeleteUnrelatedObjectsParameters = new DeleteUnrelatedObjectsParam
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };
            }
            else if (commandLine.AdditionalParameters[key].ToLower() == nameof(MaintenanceFunction.RepairAttributesAndRelations).ToLower())
            {
                Function = MaintenanceFunction.RepairAttributesAndRelations;
                RepairAttributeAndRelationsParameters = new RepairAttributeAndRelationsParam
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };
            }
            else
            {
                IsValid = false;
                ErrorMessage += $"You have to provide a valid function: [{MaintenanceFunction.CleanupNameItems}|{MaintenanceFunction.MoveClasses}] \n";
                return;
            }

            

        }

        public MaintenanceModuleController() { }

        public MaintenanceModuleController(Globals globals)
        {
            this.globals = globals;
        }
    }
}
