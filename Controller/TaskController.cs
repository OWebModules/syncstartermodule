﻿using OModules.Notifications;
using OntologyAppDBConnector;
using SyncStarterModule.Attributes;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using SyncStarterModule.Model;
using OntologyClasses.BaseClasses;

namespace SyncStarterModule.Controller
{
    public class TaskController
    {
        public delegate void TaskAddedDelegate(ModuleItem moduleItem);
        public delegate void MessageReceivedDelegate(InterServiceMessage message);
        public delegate void DataChangedDelegate();
        public delegate void TaskCompletedDelegate(ModuleItem moduleItem, clsOntologyItem result);
        public event TaskAddedDelegate TaskAdded;
        public event MessageReceivedDelegate MessageReceived;
        public event DataChangedDelegate DataChanged;
        public event TaskCompletedDelegate TaskCompleted;

        private Services.ElasticServiceAgent elasticAgent;

        private List<ModuleTask> taskList = new List<ModuleTask>();

        private InterServiceMessage lastReceivedMessage;
        public InterServiceMessage LastReceivedMessage
        {
            get
            {
                lock (objectLocker)
                {
                    return lastReceivedMessage;
                }
                
            }
            set
            {
                lock(objectLocker)
                {
                    lastReceivedMessage = value;
                    
                }
                var property = lastActivatedProperty;
                if (property != null && lastReceivedMessage.OItems != null && lastReceivedMessage.OItems.Count == 1)
                {
                    if (property.PropertyType == typeof(string))
                    {
                        var attrib = (ParamPropAttribute)property.GetCustomAttribute(typeof(ParamPropAttribute));

                        if (attrib == null || attrib.ResultProperty == ResultProp.Id)
                        {
                            property.SetValue(lastActivatedObject, lastReceivedMessage.OItems.First().GUID);
                        }
                        else
                        {
                            var oItemResult = elasticAgent.GetOItem(lastReceivedMessage.OItems.First().GUID, elasticAgent.Globals.Type_Object);
                            if (oItemResult.ResultState.GUID == elasticAgent.Globals.LState_Success.GUID)
                            {
                                property.SetValue(lastActivatedObject, oItemResult.Result.Name);
                            }
                            
                        }
                        lastActivatedObject = null;
                        lastActivatedProperty = null;
                        DataChanged?.Invoke();
                    }
                }
            }
        }

        private PropertyInfo lastActivatedProperty;
        public PropertyInfo LastActivatedProperty
        {
            get
            {
                lock(objectLocker)
                {
                    return lastActivatedProperty;
                }
            }
            set
            {
                lock(objectLocker)
                {
                    lastActivatedProperty = value;
                }
            }
        }

        private object lastActivatedObject;
        public object LastActivatedObject
        {
            get
            {
                lock (objectLocker)
                {
                    return lastActivatedObject;
                }
            }
            set
            {
                lock (objectLocker)
                {
                    lastActivatedObject = value;
                }
            }
        }

        private object objectLocker = new object();

        public void AddTask(ModuleTask moduleTask)
        {
            lock(objectLocker)
            {
                var taskInList = taskList.FirstOrDefault(tsk => tsk.ModuleItem == moduleTask.ModuleItem);
                if (taskInList != null)
                {
                    taskList.Remove(taskInList);
                }
                taskList.Add(moduleTask);
                TaskAdded?.Invoke(moduleTask.ModuleItem);
            }
            
        }

        public ModuleTask GetModuleTask(ModuleItem moduleItem)
        {
            return taskList.FirstOrDefault(tsk => tsk.ModuleItem == moduleItem);
        }

        public ModuleTask GetConnectionTask()
        {
            return taskList.FirstOrDefault(tsk => tsk.IsConnectionTask);
        }

        public bool IsConnected(ModuleItem moduleItem)
        {
            var task = taskList.FirstOrDefault(tsk => tsk.ModuleItem == moduleItem && tsk.IsConnected);
            var isIncomplete = task != null;
            return isIncomplete;
        }

        public bool IsTaskIncomplete(ModuleItem moduleItem)
        {
            var task = taskList.FirstOrDefault(tsk => tsk.ModuleItem == moduleItem && !tsk.IsConnectionTask && tsk.Task != null);
            if (task == null || (task != null && task.Task.Status == TaskStatus.RanToCompletion))
            {
                if (task != null && task.Task.Status == TaskStatus.RanToCompletion)
                {
                    TaskCompleted?.Invoke(task.ModuleItem, task.Task.Result);
                    taskList.Remove(task);
                }
                return false;
            }
            {
                return true;
            }
        }

        public TaskController(Globals globals)
        {
            elasticAgent = new Services.ElasticServiceAgent(globals);
        }

        
    }
}
