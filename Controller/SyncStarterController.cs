﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationLibrary.Controller;
using SyncStarterModule.Model;
using OModules.Controllers;
using SecurityModule.Services;
using System.Reflection;
using DatabaseManagementModule.Services;
using OntoMsg_Module.Logging;
using System.ComponentModel;

namespace SyncStarterModule.Controller
{
    public enum SyncStarterFunction
    {
        InitSyncModule,
        StartSyncModuleFunctionByConfiguration
    }
    public class SyncStarterController : BaseController
    {
        public bool DeleteMails { get; set; }
        public string MasterPassword { get; set; }

        public SyncStarterFunction Function { get; private set; }

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == "SyncStarter".ToLower();
        }

        public override string Syntax
        {
            get
            {
                return $"SyncStarter";
            }
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

           
            try
            {

                var form = new SyncStarter();
                form.ShowDialog();
                
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public SyncStarterController(CommandLine commandLine, Globals globals) : base(globals, commandLine)
        {

            IsValid = true;
            var functionParameter = commandLine.GetAdditionalParamter(nameof(Function));
            if (functionParameter.GUID == globals.LState_Error.GUID)
            {
                Function = SyncStarterFunction.InitSyncModule;
                return;
            }

            var function = functionParameter.Additional1;

            if (function == SyncStarterFunction.StartSyncModuleFunctionByConfiguration.ToString())
            {
                var emailParam = commandLine.GetAdditionalParamter("Email");
                var passwordParam = commandLine.GetAdditionalParamter("Password");

                if (emailParam.GUID == globals.LState_Error.GUID || passwordParam.GUID == globals.LState_Nothing.GUID)
                {
                    throw new Exception("You need Email-Address and a Password of the Master-User!");
                }

                if (string.IsNullOrEmpty(commandLine.IdConfig))
                {
                    throw new Exception($"You provided no IdConfig!");
                }

                var idConfig = commandLine.IdConfig;

                StaticConfig.SecurityController = new SecurityModule.SecurityController(globals);
                var loginResult = Task.Run<ResultCredentials>(() => StaticConfig.SecurityController.Login(emailParam.Additional1, passwordParam.Additional1)).GetAwaiter().GetResult();
                if (loginResult.Result.GUID == globals.LState_Error.GUID)
                {
                    throw new Exception($"Username or password is wrong!");
                }
                StaticConfig.MasterPassword = passwordParam.Additional1;

                var elasticAgent = new Services.ElasticServiceAgent(globals);

                var moduleMetasResult = elasticAgent.GetModuleMetas();
                if (moduleMetasResult.ResultState.GUID == globals.LState_Error.GUID)
                {
                    throw new Exception(moduleMetasResult.ResultState.Additional1);
                }

                var configurationMeta = moduleMetasResult.Result.ModuleMetas.FirstOrDefault(mod => mod.ModuleConfig.GUID == idConfig);
                if (configurationMeta == null)
                {
                    throw new Exception($"The configruation with Id {idConfig} cannot be found!");
                }

                var types = AppDomain.CurrentDomain.GetAssemblies().Where(ass => ass.FullName.Contains("AutomationLibrary") || ass.FullName.Contains("SyncStarterModule")).SelectMany(ass => ass.GetTypes()).ToList();
                var paramType = types.FirstOrDefault(paramT => paramT.FullName == configurationMeta.ParameterNamespace.Val_String);

                var passwordProperties = paramType.GetProperties().Where(prop => prop.GetCustomAttributes().FirstOrDefault(att => att is PasswordPropertyTextAttribute) != null);

                var paramItem = Newtonsoft.Json.JsonConvert.DeserializeObject(configurationMeta.JsonAttribute.Val_String, paramType);
                foreach (var passwordProp in passwordProperties)
                {
                    var userRel = configurationMeta.UserAuthenticationsToUsers.FirstOrDefault(rel => rel.Name_Other == passwordProp.Name);
                    if (userRel != null)
                    {
                        var getPasswordTask = Task.Run(async () =>
                        {
                            var password = await StaticConfig.SecurityController.GetPassword(new clsOntologyItem
                            {
                                GUID = userRel.ID_Other,
                                Name = userRel.Name_Other,
                                GUID_Parent = userRel.ID_Parent_Other,
                                Type = userRel.Ontology
                            }, StaticConfig.MasterPassword);

                            return password;
                        });

                        getPasswordTask.Wait();

                        if (getPasswordTask.Result.Result.GUID == globals.LState_Error.GUID)
                        {
                            throw new Exception(getPasswordTask.Result.Result.Additional1);
                        }
                        passwordProp.SetValue(paramItem, getPasswordTask.Result.CredentialItems.First().Password.Name_Other);
                    }
                }

                var controllerType = ((ModuleParamBase)paramItem).GetControllerType();
                if (controllerType == null)
                {
                    throw new Exception($"Controller of Param {paramType.ToString()} cannot be found!");
                }
                var messageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath);
                ((IModuleParam)paramItem).MessageOutput = messageOutput;

                var controller = (BaseController)Activator.CreateInstance(controllerType, paramItem, globals);
                var result = controller.DoWork();

                if (result.GUID == globals.LState_Error.GUID)
                {
                    messageOutput.OutputError("Finished with Errors!");
                    Environment.Exit(-1);
                }
                else
                {
                    messageOutput.OutputInfo("Finished successful!");
                    Environment.Exit(0);
                }

            }
            else
            {
                throw new Exception($"The provided Function {function} is not recognized!");
            }
        }        

        public SyncStarterController() { }
    }
}
