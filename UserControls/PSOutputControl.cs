﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SyncStarterModule.Controller;
using System.Windows.Forms;
using SyncStarterModule.Attributes;
using SyncStarterModule.Model;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using OModules.Notifications;
using Telerik.WinControls.UI;
using ElasticSearchNestConnector;
using System.Collections;
using System.Threading;
using AutomationLibrary.Models;
using AutomationLibrary.Controller;
using static SyncStarterModule.Controller.TaskController;
using System.Web;
using SyncStarterModule.Services;
using System.Reflection;
using Telerik.WinControls.Data;

namespace SyncStarterModule.UserControls
{
    public partial class PSOutputControl : BaseControlPropertyGrid
    {
        public event ExecuteControllerHandler ExecuteController;
        private Globals globals;

        private TaskController taskController;

        private ModuleItem moduleItem;

        private Type controllerType;
        private Type parameterType;

        private bool resultOutput = true;

        public PSOutputControl(ModuleItem moduleItem, Globals globals, TaskController taskController, Type controllerType, Type parameterType, List<ModuleMeta> moduleMetas) : base()
        {
            this.globals = globals;
            this.taskController = taskController;
            taskController.TaskCompleted += TaskController_TaskCompleted;
            this.moduleItem = moduleItem;
            this.controllerType = controllerType;
            this.parameterType = parameterType;

            InitializeComponent();

            base.Initialize(taskController, propertyGrid);

            var parameter = Activator.CreateInstance(parameterType);
            this.propertyGrid.SelectedObject = parameter;
            ((IModuleParam)parameter).MessageOutput = new UIOutput();

            LoadConfigurations(moduleMetas);
        }

        private void LoadConfigurations(List<ModuleMeta> moduleMetas)
        {
            var parameter = propertyGrid.SelectedObject;
            var type = parameter.GetType();
            var properties = type.GetProperties().ToList();

            var elasticAgent = new ElasticServiceAgent(globals);

            var getModules = moduleMetas.Where(mod => mod.Module != null && mod.Module.GUID == moduleItem.Id);

            //var dbReader = new clsUserAppDBSelector(globals.Server, globals.Port, $"syncmodules", globals.SearchRange, globals.Session);
            //var docs = dbReader.GetData_Documents(-1, moduleItem.Name, parameter.GetType().Name.ToLower(), "*");

            var listType = typeof(List<>);
            var constructedListType = listType.MakeGenericType(type);

            var configList = (IList)Activator.CreateInstance(constructedListType);

            var passwordProperties = type.GetProperties().Where(prop => prop.GetCustomAttributes().FirstOrDefault(att => att is PasswordPropertyTextAttribute) != null);

            foreach (var moduleMeta in getModules)
            {
                var paramItem = Newtonsoft.Json.JsonConvert.DeserializeObject(moduleMeta.JsonAttribute.Val_String, type);
                foreach (var passwordProp in passwordProperties)
                {
                    var userRel = moduleMeta.UserAuthenticationsToUsers.FirstOrDefault(rel => rel.Name_Other == passwordProp.Name);
                    if (userRel != null)
                    {
                        var taskResult = Task.Run(async () =>
                        {
                            var password = await StaticConfig.SecurityController.GetPassword(new clsOntologyItem
                            {
                                GUID = userRel.ID_Other,
                                Name = userRel.Name_Other,
                                GUID_Parent = userRel.ID_Parent_Other,
                                Type = userRel.Ontology
                            }, StaticConfig.MasterPassword);

                            return password;
                        });

                        taskResult.Wait();
                        if (taskResult.Result.Result.GUID == globals.LState_Error.GUID)
                        {
                            throw new Exception(taskResult.Result.Result.Additional1);
                        }
                        passwordProp.SetValue(paramItem, taskResult.Result.CredentialItems.First().Password.Name_Other);
                    }
                }

                configList.Add(paramItem);
            }

            bindingSourceConfiguration.DataSource = configList;
            radMultiColumnComboBoxConfiguration.AutoFilter = true;
            var filter = new FilterDescriptor();
            filter.PropertyName = "ConfigurationName";
            filter.Operator = FilterOperator.Contains;
            radMultiColumnComboBoxConfiguration.EditorControl.MasterTemplate.FilterDescriptors.Add(filter);
            radMultiColumnComboBoxConfiguration.Refresh();
        }

        private void TaskController_TaskCompleted(ModuleItem moduleItem, clsOntologyItem result)
        {
            if (this.InvokeRequired)
            {
                var deleg = new TaskCompletedDelegate(TaskController_TaskCompleted);
                this.Invoke(deleg, moduleItem, result);
            }
            else
            {
                if (this.moduleItem != moduleItem) return;
                var outputText = HttpUtility.HtmlEncode(result.Additional1);
                outputText = outputText.Replace("\n","</br>");

                webBrowserOutput.DocumentText = outputText;
            }
        }

        private void RadButtonExecuteModule_Click(object sender, EventArgs e)
        {
            
            var paramItem = propertyGrid.SelectedObject;
            try
            {
                var syncConnector = Activator.CreateInstance(controllerType, paramItem, globals);
                if (!((BaseController)syncConnector).IsValid)
                {
                    MessageBox.Show(((BaseController)syncConnector).ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                var controller = ((BaseController)syncConnector);
                controller.ExecuteControllerEvent += Controller_ExecuteControllerEvent;

                var task = ((BaseController)syncConnector).DoWorkAsync();
                var moduleTask = new ModuleTask(task, moduleItem, ((ModuleParamBase)paramItem).MessageOutput);
                resultOutput = false;
                taskController.AddTask(moduleTask);

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void Controller_ExecuteControllerEvent(ModuleParamBase param)
        {
            ExecuteController.Invoke(param);
        }

        private void GeminiSyncControl_Load(object sender, EventArgs e)
        {
            CheckExecutionButton();
        }

        private void CheckExecutionButton()
        {
            radButtonExecuteModule.Enabled = !taskController.IsTaskIncomplete(moduleItem);
    
            var moduleTask = taskController.GetModuleTask(moduleItem);

            if (moduleTask != null && moduleTask.Task != null)
            {
                if (moduleTask.Task.Status == TaskStatus.RanToCompletion && !resultOutput)
                {
                    resultOutput = true;
                    webBrowserOutput.DocumentText = moduleTask.Task.Result.Additional1.Replace("\n", "</br>");
                }

            }
            
        }

        private void TimerCheckRunning_Tick(object sender, EventArgs e)
        {
            CheckExecutionButton();
        }

        private void RadButtonSave_Click(object sender, EventArgs e)
        {
            var item = propertyGrid.SelectedObject;

            var elasticAgent = new Services.ElasticServiceAgent(globals);
            var getModulesMetaResult = elasticAgent.GetModuleMetas();
            if (getModulesMetaResult.ResultState.GUID == globals.LState_Error.GUID)
            {
                throw new Exception(getModulesMetaResult.ResultState.Additional1);
            }

            var saveResult = elasticAgent.SaveModuleMeta(moduleItem, item);

            if (saveResult.ResultState.GUID == globals.LState_Error.GUID)
            {
                throw new Exception(saveResult.ResultState.Additional1);
            }

            getModulesMetaResult = elasticAgent.GetModuleMetas();
            if (getModulesMetaResult.ResultState.GUID == globals.LState_Error.GUID)
            {
                throw new Exception(getModulesMetaResult.ResultState.Additional1);
            }

            LoadConfigurations(getModulesMetaResult.Result.ModuleMetas);
        }

        private void RadDropDownListConfig_TextChanged(object sender, EventArgs e)
        {
            var text = radMultiColumnComboBoxConfiguration.Text;
            ((IModuleParam)propertyGrid.SelectedObject).ConfigurationName = text;
        }

        private void RadButtonNew_Click(object sender, EventArgs e)
        {
            var parameter = Activator.CreateInstance(parameterType);
            this.propertyGrid.SelectedObject = parameter;
            ((IModuleParam)parameter).MessageOutput = new UIOutput();
            radMultiColumnComboBoxConfiguration.Text = string.Empty;

        }

        private void RadMultiColumnComboBoxConfiguration_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radMultiColumnComboBoxConfiguration.SelectedItem == null) return;
            var item = (IModuleParam)((GridViewDataRowInfo)radMultiColumnComboBoxConfiguration.SelectedItem).DataBoundItem;
            item.MessageOutput = new UIOutput();
            propertyGrid.SelectedObject = item;
        }

        private void RadButtonDeleteItem_Click(object sender, EventArgs e)
        {
            if (radMultiColumnComboBoxConfiguration.SelectedItem == null) return;
            var item = (IModuleParam)((GridViewDataRowInfo)radMultiColumnComboBoxConfiguration.SelectedItem).DataBoundItem;

            var dbReader = new clsUserAppDBSelector(globals.Server, globals.Port, $"syncmodules", globals.SearchRange, globals.Session);
            var dbDeletor = new clsUserAppDBDeletor(dbReader);

            var result = dbDeletor.DeleteItemsById($"syncmodules", item.GetType().Name.ToLower(), new List<string> { item.ConfigurationId });

        }
    }
}
