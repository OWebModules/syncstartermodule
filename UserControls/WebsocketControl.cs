﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SyncStarterModule.Controller;
using System.Windows.Forms;
using SyncStarterModule.Attributes;
using SyncStarterModule.Model;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using Microsoft.AspNet.SignalR.Client;
using System.Net;
using OModules.Notifications;
using System.Configuration;
using System.Collections.Specialized;
using System.Diagnostics;

namespace SyncStarterModule.UserControls
{
    public partial class WebsocketControl : BaseControlPropertyGrid
    {
        private Globals globals;

        private TaskController taskController;

        private ModuleItem moduleItem;

        private ChannelEndpoint endpoint;

        private clsOntologyItem userItem;

        private IHubProxy hubProxy;

        public WebsocketControl(ModuleItem moduleItem, Globals globals, TaskController taskController) : base()
        {
            this.globals = globals;
            this.taskController = taskController;
            this.moduleItem = moduleItem;

            InitializeComponent();

            radButtonSaveCredentials.Enabled = false;

            var urlResult = globals.GetConfigValue("OModulesWebsocketUrl");
            if (urlResult.GUID == globals.LState_Error.GUID)
            {
                MessageBox.Show($"Error: {urlResult.Additional1}");
                return;
            }

            var uiOutput = new UIOutput();
            var parameter = new WebsocketParam
            {
                MessageOutput = uiOutput,
                WebsocketUrl = urlResult.Additional1
            };

            propertyGrid.SelectedObject = parameter;

            taskController.MessageReceived += TaskController_MessageReceived;
            propertyGrid.EditorInitialized += PropertyGrid_EditorInitialized;

            LoadConfiguration();
        }

        private void LoadConfiguration()
        {
            //Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var websocketParam = (WebsocketParam)propertyGrid.SelectedObject;
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = ((AppSettingsSection)config.GetSection("Authentication")).Settings;

            var userName = settings["Username"].Value;
            var password = settings["Password"].Value;

            if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(password))
            {
                websocketParam.User = userName;
                websocketParam.Password = password;
                Task.Run(() => Connect()).Wait();
            }
        }

        private void PropertyGrid_EditorInitialized(object sender, Telerik.WinControls.UI.PropertyGridItemEditorInitializedEventArgs e)
        {
            
        }

        private void TaskController_MessageReceived(InterServiceMessage message)
        {
            var activeEditor = propertyGrid.ActiveEditor;

            if (activeEditor != null)
            {
                
            }
        }

        public void SendInterServiceMessage(string idClass, string nameClass)
        {
            var interServiceMessage = new InterServiceMessage
            {
                ChannelId = OModules.Channels.LocalData.Object_SelectedClassNode.GUID,
                SessionId = endpoint.SessionId,
                UserId = endpoint.UserId,
                SenderId = endpoint.EndPointId,
                OItems = new List<clsOntologyItem>
                {
                    new  clsOntologyItem
                    {
                        GUID = idClass,
                        Name = nameClass
                    }
                }
            };
            hubProxy.Invoke("SendInterServiceMessage", interServiceMessage);
        }

        private async void RadButtonConnect_Click(object sender, EventArgs e)
        {
            await Connect();
        }

        private async Task Connect()
        {
            radButtonSaveCredentials.Enabled = false;
            try
            {
                var parameter = (WebsocketParam)propertyGrid.SelectedObject;

                var validationResult = parameter.ValidateParam(globals);

                if (validationResult.GUID == globals.LState_Error.GUID)
                {
                    throw new Exception($"The Parameter is not valid: {validationResult.Additional1}");
                }

                var validateLoginResult = await parameter.ValidateLogin(globals);
                if (validateLoginResult.GUID == globals.LState_Error.GUID)
                {
                    throw new Exception($"{validateLoginResult.Additional1}");
                }

                userItem = parameter.UserItem;

                var hubConnection = new HubConnection(parameter.WebsocketUrl);
                hubConnection.Credentials = new NetworkCredential(parameter.User, parameter.Password);
                hubProxy = hubConnection.CreateHubProxy("ModuleComHub");
                hubProxy.Subscribe("addNewMessageToPage");

                hubProxy.On<string, InterServiceMessage>("addNewMessageToPage", (name, message) =>
                {
                    if (message.ChannelId == OModules.Channels.LocalData.Object_AppliedObjects.GUID)
                    {
                        taskController.LastReceivedMessage = message;
                    }
                });
                hubConnection.Start().Wait();

                endpoint = new ChannelEndpoint
                {
                    ChannelTypeId = OModules.Channels.LocalData.Object_AppliedObjects.GUID,
                    EndPointId = Guid.NewGuid().ToString(),
                    EndpointType = EndpointType.Receiver,
                    SessionId = Guid.NewGuid().ToString(),
                    UserId = userItem.GUID
                };

                await hubProxy.Invoke("RegisterEndpoint", endpoint);

                var moduleTask = new ModuleTask(null, moduleItem, parameter.MessageOutput, isConnectionTask: true);
                moduleTask.IsConnected = true;
                taskController.AddTask(moduleTask);
                radButtonSaveCredentials.Enabled = true;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void GeminiSyncControl_Load(object sender, EventArgs e)
        {
            CheckExecutionButton();
        }

        private void CheckExecutionButton()
        {
            radButtonConnect.Enabled = !taskController.IsConnected(moduleItem);
        }

        private void TimerCheckRunning_Tick(object sender, EventArgs e)
        {
            CheckExecutionButton();
        }

        private void RadButtonSaveCredentials_Click(object sender, EventArgs e)
        {
            var websocketParam = (WebsocketParam)propertyGrid.SelectedObject;
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = ((AppSettingsSection)config.GetSection("Authentication")).Settings;

            settings["Username"].Value = websocketParam.User;
            settings["Password"].Value = websocketParam.Password;
            config.Save();
            MessageBox.Show("Credentials are saved");
        }

        private void RadButtonOpenBrowser_Click(object sender, EventArgs e)
        {
            var websocketParam = (WebsocketParam)propertyGrid.SelectedObject;

            if (!string.IsNullOrEmpty(websocketParam.WebsocketUrl))
            {
                var process = new Process();
                process.StartInfo.FileName = websocketParam.WebsocketUrl;
                process.Start();
            }
        }
    }
}
