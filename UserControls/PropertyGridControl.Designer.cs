﻿using SyncStarterModule.Model;

namespace SyncStarterModule.UserControls
{
    partial class PropertyGridControl
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor1 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.propertyGrid = new Telerik.WinControls.UI.RadPropertyGrid();
            this.radButtonExecuteModule = new Telerik.WinControls.UI.RadButton();
            this.timerCheckRunning = new System.Windows.Forms.Timer(this.components);
            this.radLabelName = new Telerik.WinControls.UI.RadLabel();
            this.radMultiColumnComboBoxConfiguration = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.bindingSourceConfiguration = new System.Windows.Forms.BindingSource(this.components);
            this.radButtonDeleteItem = new Telerik.WinControls.UI.RadButton();
            this.radButtonNew = new Telerik.WinControls.UI.RadButton();
            this.radButtonSave = new Telerik.WinControls.UI.RadButton();
            this.radButtonStopExecution = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.propertyGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonExecuteModule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBoxConfiguration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBoxConfiguration.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBoxConfiguration.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceConfiguration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonDeleteItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonStopExecution)).BeginInit();
            this.SuspendLayout();
            // 
            // propertyGrid
            // 
            this.propertyGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.propertyGrid.Location = new System.Drawing.Point(0, 28);
            this.propertyGrid.Name = "propertyGrid";
            this.propertyGrid.PropertySort = System.Windows.Forms.PropertySort.Alphabetical;
            this.propertyGrid.Size = new System.Drawing.Size(536, 413);
            this.propertyGrid.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.propertyGrid.TabIndex = 0;
            this.propertyGrid.ThemeName = "ControlDefault";
            this.propertyGrid.ToolbarVisible = true;
            // 
            // radButtonExecuteModule
            // 
            this.radButtonExecuteModule.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radButtonExecuteModule.Location = new System.Drawing.Point(417, 448);
            this.radButtonExecuteModule.Name = "radButtonExecuteModule";
            this.radButtonExecuteModule.Size = new System.Drawing.Size(110, 24);
            this.radButtonExecuteModule.TabIndex = 1;
            this.radButtonExecuteModule.Text = "Execute Module";
            this.radButtonExecuteModule.Click += new System.EventHandler(this.RadButtonExecuteModule_Click);
            // 
            // timerCheckRunning
            // 
            this.timerCheckRunning.Enabled = true;
            this.timerCheckRunning.Interval = 400;
            this.timerCheckRunning.Tick += new System.EventHandler(this.TimerCheckRunning_Tick);
            // 
            // radLabelName
            // 
            this.radLabelName.Location = new System.Drawing.Point(5, 4);
            this.radLabelName.Name = "radLabelName";
            this.radLabelName.Size = new System.Drawing.Size(39, 18);
            this.radLabelName.TabIndex = 2;
            this.radLabelName.Text = "Name:";
            // 
            // radMultiColumnComboBoxConfiguration
            // 
            this.radMultiColumnComboBoxConfiguration.DataSource = this.bindingSourceConfiguration;
            // 
            // radMultiColumnComboBoxConfiguration.NestedRadGridView
            // 
            this.radMultiColumnComboBoxConfiguration.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.radMultiColumnComboBoxConfiguration.EditorControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.radMultiColumnComboBoxConfiguration.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.radMultiColumnComboBoxConfiguration.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radMultiColumnComboBoxConfiguration.EditorControl.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.radMultiColumnComboBoxConfiguration.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.radMultiColumnComboBoxConfiguration.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.radMultiColumnComboBoxConfiguration.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.radMultiColumnComboBoxConfiguration.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.radMultiColumnComboBoxConfiguration.EditorControl.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn1.EnableExpressionEditor = false;
            gridViewTextBoxColumn1.FieldName = "ConfigurationName";
            gridViewTextBoxColumn1.HeaderText = "ConfigurationName";
            gridViewTextBoxColumn1.Name = "ConfigurationName";
            gridViewTextBoxColumn1.SortOrder = Telerik.WinControls.UI.RadSortOrder.Ascending;
            gridViewTextBoxColumn1.Width = 277;
            gridViewTextBoxColumn2.EnableExpressionEditor = false;
            gridViewTextBoxColumn2.FieldName = "ConfigurationId";
            gridViewTextBoxColumn2.HeaderText = "ConfigurationId";
            gridViewTextBoxColumn2.Name = "ConfigurationId";
            gridViewTextBoxColumn2.Width = 252;
            this.radMultiColumnComboBoxConfiguration.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.radMultiColumnComboBoxConfiguration.EditorControl.MasterTemplate.DataSource = this.bindingSourceConfiguration;
            this.radMultiColumnComboBoxConfiguration.EditorControl.MasterTemplate.EnableFiltering = true;
            this.radMultiColumnComboBoxConfiguration.EditorControl.MasterTemplate.EnableGrouping = false;
            this.radMultiColumnComboBoxConfiguration.EditorControl.MasterTemplate.ShowFilteringRow = false;
            sortDescriptor1.PropertyName = "ConfigurationName";
            this.radMultiColumnComboBoxConfiguration.EditorControl.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor1});
            this.radMultiColumnComboBoxConfiguration.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radMultiColumnComboBoxConfiguration.EditorControl.Name = "NestedRadGridView";
            this.radMultiColumnComboBoxConfiguration.EditorControl.ReadOnly = true;
            this.radMultiColumnComboBoxConfiguration.EditorControl.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radMultiColumnComboBoxConfiguration.EditorControl.ShowGroupPanel = false;
            this.radMultiColumnComboBoxConfiguration.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.radMultiColumnComboBoxConfiguration.EditorControl.TabIndex = 0;
            this.radMultiColumnComboBoxConfiguration.Location = new System.Drawing.Point(52, 3);
            this.radMultiColumnComboBoxConfiguration.Name = "radMultiColumnComboBoxConfiguration";
            this.radMultiColumnComboBoxConfiguration.Size = new System.Drawing.Size(393, 20);
            this.radMultiColumnComboBoxConfiguration.TabIndex = 6;
            this.radMultiColumnComboBoxConfiguration.TabStop = false;
            this.radMultiColumnComboBoxConfiguration.SelectedIndexChanged += new System.EventHandler(this.RadMultiColumnComboBoxConfiguration_SelectedIndexChanged);
            // 
            // radButtonDeleteItem
            // 
            this.radButtonDeleteItem.Image = global::SyncStarterModule.Properties.Resources.DeleteListItem_16x;
            this.radButtonDeleteItem.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButtonDeleteItem.Location = new System.Drawing.Point(479, 1);
            this.radButtonDeleteItem.Name = "radButtonDeleteItem";
            this.radButtonDeleteItem.Size = new System.Drawing.Size(27, 24);
            this.radButtonDeleteItem.TabIndex = 7;
            this.radButtonDeleteItem.Click += new System.EventHandler(this.RadButtonDeleteItem_Click);
            // 
            // radButtonNew
            // 
            this.radButtonNew.Image = global::SyncStarterModule.Properties.Resources.NewFile_16x;
            this.radButtonNew.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButtonNew.Location = new System.Drawing.Point(508, 1);
            this.radButtonNew.Name = "radButtonNew";
            this.radButtonNew.Size = new System.Drawing.Size(27, 24);
            this.radButtonNew.TabIndex = 5;
            this.radButtonNew.Click += new System.EventHandler(this.RadButtonNew_Click);
            // 
            // radButtonSave
            // 
            this.radButtonSave.Image = global::SyncStarterModule.Properties.Resources.Save_16x;
            this.radButtonSave.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButtonSave.Location = new System.Drawing.Point(450, 1);
            this.radButtonSave.Name = "radButtonSave";
            this.radButtonSave.Size = new System.Drawing.Size(27, 24);
            this.radButtonSave.TabIndex = 4;
            this.radButtonSave.Click += new System.EventHandler(this.RadButtonSave_Click);
            // 
            // radButtonStopExecution
            // 
            this.radButtonStopExecution.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radButtonStopExecution.Location = new System.Drawing.Point(301, 448);
            this.radButtonStopExecution.Name = "radButtonStopExecution";
            this.radButtonStopExecution.Size = new System.Drawing.Size(110, 24);
            this.radButtonStopExecution.TabIndex = 8;
            this.radButtonStopExecution.Text = "Stop";
            this.radButtonStopExecution.Visible = false;
            this.radButtonStopExecution.Click += new System.EventHandler(this.RadButtonStopExecution_Click);
            // 
            // PropertyGridControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radButtonStopExecution);
            this.Controls.Add(this.radButtonDeleteItem);
            this.Controls.Add(this.radMultiColumnComboBoxConfiguration);
            this.Controls.Add(this.radButtonNew);
            this.Controls.Add(this.radButtonSave);
            this.Controls.Add(this.radLabelName);
            this.Controls.Add(this.radButtonExecuteModule);
            this.Controls.Add(this.propertyGrid);
            this.Name = "PropertyGridControl";
            this.Size = new System.Drawing.Size(536, 479);
            this.Load += new System.EventHandler(this.GeminiSyncControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.propertyGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonExecuteModule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBoxConfiguration.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBoxConfiguration.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBoxConfiguration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceConfiguration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonDeleteItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonStopExecution)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadPropertyGrid propertyGrid;
        private Telerik.WinControls.UI.RadButton radButtonExecuteModule;
        private System.Windows.Forms.Timer timerCheckRunning;
        private Telerik.WinControls.UI.RadLabel radLabelName;
        private Telerik.WinControls.UI.RadButton radButtonSave;
        private System.Windows.Forms.BindingSource bindingSourceConfiguration;
        private Telerik.WinControls.UI.RadButton radButtonNew;
        private Telerik.WinControls.UI.RadMultiColumnComboBox radMultiColumnComboBoxConfiguration;
        private Telerik.WinControls.UI.RadButton radButtonDeleteItem;
        private Telerik.WinControls.UI.RadButton radButtonStopExecution;
    }
}
