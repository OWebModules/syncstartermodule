﻿using SyncStarterModule.Model;

namespace SyncStarterModule.UserControls
{
    partial class WebsocketControl
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.propertyGrid = new Telerik.WinControls.UI.RadPropertyGrid();
            this.radButtonConnect = new Telerik.WinControls.UI.RadButton();
            this.timerCheckRunning = new System.Windows.Forms.Timer(this.components);
            this.radButtonSaveCredentials = new Telerik.WinControls.UI.RadButton();
            this.radButtonOpenBrowser = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.propertyGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonConnect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonSaveCredentials)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonOpenBrowser)).BeginInit();
            this.SuspendLayout();
            // 
            // propertyGrid
            // 
            this.propertyGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.propertyGrid.Location = new System.Drawing.Point(0, 0);
            this.propertyGrid.Name = "propertyGrid";
            this.propertyGrid.PropertySort = System.Windows.Forms.PropertySort.Alphabetical;
            this.propertyGrid.Size = new System.Drawing.Size(536, 441);
            this.propertyGrid.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.propertyGrid.TabIndex = 0;
            this.propertyGrid.ThemeName = "ControlDefault";
            this.propertyGrid.ToolbarVisible = true;
            // 
            // radButtonConnect
            // 
            this.radButtonConnect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radButtonConnect.Location = new System.Drawing.Point(298, 447);
            this.radButtonConnect.Name = "radButtonConnect";
            this.radButtonConnect.Size = new System.Drawing.Size(110, 24);
            this.radButtonConnect.TabIndex = 1;
            this.radButtonConnect.Text = "Connect";
            this.radButtonConnect.Click += new System.EventHandler(this.RadButtonConnect_Click);
            // 
            // timerCheckRunning
            // 
            this.timerCheckRunning.Enabled = true;
            this.timerCheckRunning.Interval = 300;
            this.timerCheckRunning.Tick += new System.EventHandler(this.TimerCheckRunning_Tick);
            // 
            // radButtonSaveCredentials
            // 
            this.radButtonSaveCredentials.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radButtonSaveCredentials.Location = new System.Drawing.Point(415, 447);
            this.radButtonSaveCredentials.Name = "radButtonSaveCredentials";
            this.radButtonSaveCredentials.Size = new System.Drawing.Size(110, 24);
            this.radButtonSaveCredentials.TabIndex = 2;
            this.radButtonSaveCredentials.Text = "Save Credentials";
            this.radButtonSaveCredentials.Click += new System.EventHandler(this.RadButtonSaveCredentials_Click);
            // 
            // radButtonOpenBrowser
            // 
            this.radButtonOpenBrowser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radButtonOpenBrowser.Image = global::SyncStarterModule.Properties.Resources.Web_16x;
            this.radButtonOpenBrowser.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButtonOpenBrowser.Location = new System.Drawing.Point(3, 447);
            this.radButtonOpenBrowser.Name = "radButtonOpenBrowser";
            this.radButtonOpenBrowser.Size = new System.Drawing.Size(30, 24);
            this.radButtonOpenBrowser.TabIndex = 3;
            this.radButtonOpenBrowser.Click += new System.EventHandler(this.RadButtonOpenBrowser_Click);
            // 
            // WebsocketControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radButtonOpenBrowser);
            this.Controls.Add(this.radButtonSaveCredentials);
            this.Controls.Add(this.radButtonConnect);
            this.Controls.Add(this.propertyGrid);
            this.Name = "WebsocketControl";
            this.Size = new System.Drawing.Size(536, 479);
            this.Load += new System.EventHandler(this.GeminiSyncControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.propertyGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonConnect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonSaveCredentials)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonOpenBrowser)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPropertyGrid propertyGrid;
        private Telerik.WinControls.UI.RadButton radButtonConnect;
        private System.Windows.Forms.Timer timerCheckRunning;
        private Telerik.WinControls.UI.RadButton radButtonSaveCredentials;
        private Telerik.WinControls.UI.RadButton radButtonOpenBrowser;
    }
}
