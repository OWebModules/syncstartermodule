﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SyncStarterModule.Controller;
using System.Windows.Forms;
using SyncStarterModule.Attributes;
using SyncStarterModule.Model;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using OModules.Notifications;
using Telerik.WinControls.UI;
using ElasticSearchNestConnector;
using System.Collections;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Threading;
using SyncStarterModule.Services;
using Newtonsoft.Json;
using System.Web;
using RabbitMQModule.Models;
using OntoMsg_Module.Logging;
using AutomationLibrary.Models;
using AutomationLibrary.Controller;

namespace SyncStarterModule.UserControls
{
    public partial class RabbitMQMessageControl : BaseControlPropertyGrid
    {
        public ExecuteControllerHandler ExecuteControllerEvent;
        private Globals globals;

        private TaskController taskController;

        private ModuleItem moduleItem;

        private Type controllerType;
        private Type parameterType;

        public RabbitMQMessageControl(ModuleItem moduleItem, Globals globals, TaskController taskController, Type controllerType, Type parameterType) : base()
        {
            this.globals = globals;
            this.taskController = taskController;
            this.moduleItem = moduleItem;
            this.controllerType = controllerType;
            this.parameterType = parameterType;

            InitializeComponent();

            base.Initialize(taskController, propertyGrid);

            var parameter = Activator.CreateInstance(parameterType);
            this.propertyGrid.SelectedObject = parameter;
            ((IModuleParam)parameter).MessageOutput = new UIOutput();

            if (parameter.GetType().GetProperties().Any(prop => prop.PropertyType == typeof(CancellationTokenSource)))
            {
                radButtonStopExecution.Visible = true;
                radButtonStopExecution.Enabled = false;
            }
            else
            {
                radButtonStopExecution.Visible = false;
            }

            LoadConfigurations();
        }

        private void LoadConfigurations()
        {
            var parameter = propertyGrid.SelectedObject;
            var type = parameter.GetType();
            var properties = type.GetProperties().ToList();

            var elasticAgent = new ElasticServiceAgent(globals);

            var getModules = elasticAgent.GetModuleMetas(moduleItem.Id);

            if (getModules.ResultState.GUID == globals.LState_Error.GUID)
            {
                UpdateNecessaryOntologies();
                getModules = elasticAgent.GetModuleMetas(moduleItem.Id);
                if (getModules.ResultState.GUID == globals.LState_Error.GUID)
                {
                    throw new Exception(getModules.ResultState.Additional1);
                }
            }
            //var dbReader = new clsUserAppDBSelector(globals.Server, globals.Port, $"syncmodules", globals.SearchRange, globals.Session);
            //var docs = dbReader.GetData_Documents(-1, moduleItem.Name, parameter.GetType().Name.ToLower(), "*");

            var listType = typeof(List<>);
            var constructedListType = listType.MakeGenericType(type);

            var configList = (IList)Activator.CreateInstance(constructedListType);

            foreach (var moduleMeta in getModules.Result.ModuleMetas)
            {
                configList.Add(Newtonsoft.Json.JsonConvert.DeserializeObject(moduleMeta.JsonAttribute.Val_String, type));
            }

            bindingSourceConfiguration.DataSource = configList;
            radMultiColumnComboBoxConfiguration.Refresh();
        }

        private void UpdateNecessaryOntologies()
        {
            var param = new UpdateOntologiesParam
            {
                RootPath = AppDomain.CurrentDomain.BaseDirectory,
                MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
            };

            var controller = new FileSystemModuleController(param, globals);
            var task = Task.Run<clsOntologyItem>(() => controller.DoWorkAsync());
            task.Wait();


        }

        private void RadButtonExecuteModule_Click(object sender, EventArgs e)
        {

            var paramItem = propertyGrid.SelectedObject;
            try
            {
                var syncConnector = (RabbitMQController) Activator.CreateInstance(controllerType, paramItem, globals);
                if (!((BaseController)syncConnector).IsValid)
                {
                    MessageBox.Show(((BaseController)syncConnector).ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                syncConnector.ReceivedStartModuleMessage += SyncConnector_ReceivedStartModuleMessage;

                var controller = (BaseController)syncConnector;
                controller.ExecuteControllerEvent += Controller_ExecuteControllerEvent;
                var task = ((BaseController)syncConnector).DoWorkAsync();
                var moduleTask = new ModuleTask(task, moduleItem, ((ModuleParamBase)paramItem).MessageOutput);
                taskController.AddTask(moduleTask);

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void Controller_ExecuteControllerEvent(ModuleParamBase param)
        {
            ExecuteControllerEvent?.Invoke(param);
        }

        private clsOntologyItem SyncConnector_ReceivedStartModuleMessage(List<RabbitMQModule.Models.StartModuleMessage> moduleMessage)
        {
            if (this.InvokeRequired)
            {
                var deleg = new ReceivedStartModuleMessageHandler(SyncConnector_ReceivedStartModuleMessage);
                var result = this.Invoke(deleg, moduleMessage);

                foreach (var message in moduleMessage)
                {
                    
                }
                return (clsOntologyItem)result;
            }
            else
            {
                foreach (var message in moduleMessage)
                {
                    var json = HttpUtility.HtmlEncode(Newtonsoft.Json.JsonConvert.SerializeObject(message, Formatting.Indented));
                    webBrowserOutput.DocumentText = json;
                }
                return globals.LState_Success.Clone();
            }
        }

        private void GeminiSyncControl_Load(object sender, EventArgs e)
        {
            CheckExecutionButton();
        }

        private void CheckExecutionButton()
        {
            radButtonExecuteModule.Enabled = !taskController.IsTaskIncomplete(moduleItem);
            radButtonStopExecution.Enabled = taskController.IsTaskIncomplete(moduleItem);
        }

        private void TimerCheckRunning_Tick(object sender, EventArgs e)
        {
            CheckExecutionButton();
        }

        private void RadButtonSave_Click(object sender, EventArgs e)
        {
            var item = propertyGrid.SelectedObject;

            var elasticAgent = new Services.ElasticServiceAgent(globals);

            var saveResult = elasticAgent.SaveModuleMeta(moduleItem, item);

            if (saveResult.ResultState.GUID == globals.LState_Error.GUID)
            {
                throw new Exception(saveResult.ResultState.Additional1);
            }

            LoadConfigurations();
        }

        private void RadDropDownListConfig_TextChanged(object sender, EventArgs e)
        {
            var text = radMultiColumnComboBoxConfiguration.Text;
            ((IModuleParam)propertyGrid.SelectedObject).ConfigurationName = text;
        }

        private void RadButtonNew_Click(object sender, EventArgs e)
        {
            var parameter = Activator.CreateInstance(parameterType);
            this.propertyGrid.SelectedObject = parameter;
            ((IModuleParam)parameter).MessageOutput = new UIOutput();
            radMultiColumnComboBoxConfiguration.Text = string.Empty;

        }

        private void RadMultiColumnComboBoxConfiguration_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radMultiColumnComboBoxConfiguration.SelectedItem == null) return;
            var item = (IModuleParam)((GridViewDataRowInfo)radMultiColumnComboBoxConfiguration.SelectedItem).DataBoundItem;
            item.MessageOutput = new UIOutput();
            propertyGrid.SelectedObject = item;
        }

        private void RadButtonDeleteItem_Click(object sender, EventArgs e)
        {
            if (radMultiColumnComboBoxConfiguration.SelectedItem == null) return;
            var item = (IModuleParam)((GridViewDataRowInfo)radMultiColumnComboBoxConfiguration.SelectedItem).DataBoundItem;

            var dbReader = new clsUserAppDBSelector(globals.Server, globals.Port, $"syncmodules", globals.SearchRange, globals.Session);
            var dbDeletor = new clsUserAppDBDeletor(dbReader);

            var result = dbDeletor.DeleteItemsById($"syncmodules", item.GetType().Name.ToLower(), new List<string> { item.ConfigurationId });

        }

        private void RadButtonStopExecution_Click(object sender, EventArgs e)
        {
            var parameter = propertyGrid.SelectedObject;
            var property = parameter.GetType().GetProperties().FirstOrDefault(prop => prop.PropertyType == typeof(CancellationTokenSource));
            if (property == null)
            {
                radButtonStopExecution.Enabled = false;
                return;
            }

            var cancellationTokenSource = (CancellationTokenSource)property.GetValue(parameter);
            cancellationTokenSource.Cancel();
        }
    }
}
