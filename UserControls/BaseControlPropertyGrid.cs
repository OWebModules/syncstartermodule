﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SyncStarterModule.Model;
using SyncStarterModule.Attributes;
using Telerik.WinControls.UI;
using SyncStarterModule.Controller;
using static SyncStarterModule.Controller.TaskController;

namespace SyncStarterModule.UserControls
{
    public partial class BaseControlPropertyGrid : UserControl
    {
        public event SendMessageDelegate SendMessage;
        private TaskController taskController;
        private RadPropertyGrid propertyGrid;

        protected void SendInterserviceMessage(string idClass, string nameClass)
        {
            SendMessage?.Invoke(idClass, nameClass);
        }

        protected void SelectedProperty(RadPropertyGrid propertyGrid, TaskController taskController, string propertyName)
        {
            var property = propertyGrid.SelectedObject.GetType().GetProperty(propertyName);
            taskController.LastActivatedObject = propertyGrid.SelectedObject;
            taskController.LastActivatedProperty = property;
            var attribute = (ParamPropAttribute)property.GetCustomAttributes(false).FirstOrDefault(attr => attr is ParamPropAttribute);
            if (attribute == null) return;

            var idClass = attribute.IdClass;
            var nameClass = attribute.NameClass;

            if (string.IsNullOrEmpty(idClass)) return;

            SendInterserviceMessage(idClass, nameClass);
        }

        protected void Initialize(TaskController taskController, RadPropertyGrid propertyGrid)
        {
            this.taskController = taskController;
            this.propertyGrid = propertyGrid;

            this.taskController.DataChanged += TaskController_DataChanged;
            propertyGrid.EditorInitialized += PropertyGrid_EditorInitialized;
        }

        public virtual void FilterConfigs(string idFunctionItem)
        {
            throw new NotImplementedException();
        }

        private void PropertyGrid_EditorInitialized(object sender, Telerik.WinControls.UI.PropertyGridItemEditorInitializedEventArgs e)
        {
            SelectedProperty(propertyGrid, taskController, e.Item.Name);
        }

        private void TaskController_DataChanged()
        {
            if (InvokeRequired)
            {
                var deleg = new DataChangedDelegate(TaskController_DataChanged);
                this.Invoke(deleg);
            }
            else
            {
                propertyGrid.Refresh();
            }

        }

        public BaseControlPropertyGrid()
        {
            InitializeComponent();

        }
    }
}
