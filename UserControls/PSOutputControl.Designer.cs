﻿using SyncStarterModule.Model;

namespace SyncStarterModule.UserControls
{
    partial class PSOutputControl
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.timerCheckRunning = new System.Windows.Forms.Timer(this.components);
            this.bindingSourceConfiguration = new System.Windows.Forms.BindingSource(this.components);
            this.radButtonDeleteItem = new Telerik.WinControls.UI.RadButton();
            this.radMultiColumnComboBoxConfiguration = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.radButtonNew = new Telerik.WinControls.UI.RadButton();
            this.radButtonSave = new Telerik.WinControls.UI.RadButton();
            this.radLabelName = new Telerik.WinControls.UI.RadLabel();
            this.radButtonExecuteModule = new Telerik.WinControls.UI.RadButton();
            this.tabControlContent = new System.Windows.Forms.TabControl();
            this.tabPageParameters = new System.Windows.Forms.TabPage();
            this.tabPageWebbrowser = new System.Windows.Forms.TabPage();
            this.propertyGrid = new Telerik.WinControls.UI.RadPropertyGrid();
            this.webBrowserOutput = new System.Windows.Forms.WebBrowser();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceConfiguration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonDeleteItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBoxConfiguration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBoxConfiguration.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBoxConfiguration.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonExecuteModule)).BeginInit();
            this.tabControlContent.SuspendLayout();
            this.tabPageParameters.SuspendLayout();
            this.tabPageWebbrowser.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.propertyGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // timerCheckRunning
            // 
            this.timerCheckRunning.Enabled = true;
            this.timerCheckRunning.Interval = 400;
            this.timerCheckRunning.Tick += new System.EventHandler(this.TimerCheckRunning_Tick);
            // 
            // radButtonDeleteItem
            // 
            this.radButtonDeleteItem.Image = global::SyncStarterModule.Properties.Resources.DeleteListItem_16x;
            this.radButtonDeleteItem.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButtonDeleteItem.Location = new System.Drawing.Point(479, 1);
            this.radButtonDeleteItem.Name = "radButtonDeleteItem";
            this.radButtonDeleteItem.Size = new System.Drawing.Size(27, 24);
            this.radButtonDeleteItem.TabIndex = 7;
            this.radButtonDeleteItem.Click += new System.EventHandler(this.RadButtonDeleteItem_Click);
            // 
            // radMultiColumnComboBoxConfiguration
            // 
            this.radMultiColumnComboBoxConfiguration.DataSource = this.bindingSourceConfiguration;
            // 
            // radMultiColumnComboBoxConfiguration.NestedRadGridView
            // 
            this.radMultiColumnComboBoxConfiguration.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.radMultiColumnComboBoxConfiguration.EditorControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.radMultiColumnComboBoxConfiguration.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.radMultiColumnComboBoxConfiguration.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radMultiColumnComboBoxConfiguration.EditorControl.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.radMultiColumnComboBoxConfiguration.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.radMultiColumnComboBoxConfiguration.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.radMultiColumnComboBoxConfiguration.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.radMultiColumnComboBoxConfiguration.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.radMultiColumnComboBoxConfiguration.EditorControl.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn1.EnableExpressionEditor = false;
            gridViewTextBoxColumn1.FieldName = "ConfigurationId";
            gridViewTextBoxColumn1.HeaderText = "ConfigurationId";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "ConfigurationId";
            gridViewTextBoxColumn2.EnableExpressionEditor = false;
            gridViewTextBoxColumn2.FieldName = "ConfigurationName";
            gridViewTextBoxColumn2.HeaderText = "ConfigurationName";
            gridViewTextBoxColumn2.Name = "ConfigurationName";
            gridViewTextBoxColumn2.Width = 277;
            this.radMultiColumnComboBoxConfiguration.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.radMultiColumnComboBoxConfiguration.EditorControl.MasterTemplate.DataSource = this.bindingSourceConfiguration;
            this.radMultiColumnComboBoxConfiguration.EditorControl.MasterTemplate.EnableGrouping = false;
            this.radMultiColumnComboBoxConfiguration.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.radMultiColumnComboBoxConfiguration.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radMultiColumnComboBoxConfiguration.EditorControl.Name = "NestedRadGridView";
            this.radMultiColumnComboBoxConfiguration.EditorControl.ReadOnly = true;
            this.radMultiColumnComboBoxConfiguration.EditorControl.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radMultiColumnComboBoxConfiguration.EditorControl.ShowGroupPanel = false;
            this.radMultiColumnComboBoxConfiguration.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.radMultiColumnComboBoxConfiguration.EditorControl.TabIndex = 0;
            this.radMultiColumnComboBoxConfiguration.Location = new System.Drawing.Point(52, 3);
            this.radMultiColumnComboBoxConfiguration.Name = "radMultiColumnComboBoxConfiguration";
            this.radMultiColumnComboBoxConfiguration.Size = new System.Drawing.Size(393, 20);
            this.radMultiColumnComboBoxConfiguration.TabIndex = 6;
            this.radMultiColumnComboBoxConfiguration.TabStop = false;
            this.radMultiColumnComboBoxConfiguration.SelectedIndexChanged += new System.EventHandler(this.RadMultiColumnComboBoxConfiguration_SelectedIndexChanged);
            // 
            // radButtonNew
            // 
            this.radButtonNew.Image = global::SyncStarterModule.Properties.Resources.NewFile_16x;
            this.radButtonNew.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButtonNew.Location = new System.Drawing.Point(508, 1);
            this.radButtonNew.Name = "radButtonNew";
            this.radButtonNew.Size = new System.Drawing.Size(27, 24);
            this.radButtonNew.TabIndex = 5;
            this.radButtonNew.Click += new System.EventHandler(this.RadButtonNew_Click);
            // 
            // radButtonSave
            // 
            this.radButtonSave.Image = global::SyncStarterModule.Properties.Resources.Save_16x;
            this.radButtonSave.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButtonSave.Location = new System.Drawing.Point(450, 1);
            this.radButtonSave.Name = "radButtonSave";
            this.radButtonSave.Size = new System.Drawing.Size(27, 24);
            this.radButtonSave.TabIndex = 4;
            this.radButtonSave.Click += new System.EventHandler(this.RadButtonSave_Click);
            // 
            // radLabelName
            // 
            this.radLabelName.Location = new System.Drawing.Point(5, 4);
            this.radLabelName.Name = "radLabelName";
            this.radLabelName.Size = new System.Drawing.Size(39, 18);
            this.radLabelName.TabIndex = 2;
            this.radLabelName.Text = "Name:";
            // 
            // radButtonExecuteModule
            // 
            this.radButtonExecuteModule.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radButtonExecuteModule.Location = new System.Drawing.Point(417, 399);
            this.radButtonExecuteModule.Name = "radButtonExecuteModule";
            this.radButtonExecuteModule.Size = new System.Drawing.Size(110, 24);
            this.radButtonExecuteModule.TabIndex = 1;
            this.radButtonExecuteModule.Text = "Execute Module";
            this.radButtonExecuteModule.Click += new System.EventHandler(this.RadButtonExecuteModule_Click);
            // 
            // tabControlContent
            // 
            this.tabControlContent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlContent.Controls.Add(this.tabPageParameters);
            this.tabControlContent.Controls.Add(this.tabPageWebbrowser);
            this.tabControlContent.Location = new System.Drawing.Point(5, 28);
            this.tabControlContent.Name = "tabControlContent";
            this.tabControlContent.SelectedIndex = 0;
            this.tabControlContent.Size = new System.Drawing.Size(528, 365);
            this.tabControlContent.TabIndex = 8;
            // 
            // tabPageParameters
            // 
            this.tabPageParameters.Controls.Add(this.propertyGrid);
            this.tabPageParameters.Location = new System.Drawing.Point(4, 22);
            this.tabPageParameters.Name = "tabPageParameters";
            this.tabPageParameters.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageParameters.Size = new System.Drawing.Size(520, 339);
            this.tabPageParameters.TabIndex = 0;
            this.tabPageParameters.Text = "Parameters";
            this.tabPageParameters.UseVisualStyleBackColor = true;
            // 
            // tabPageWebbrowser
            // 
            this.tabPageWebbrowser.Controls.Add(this.webBrowserOutput);
            this.tabPageWebbrowser.Location = new System.Drawing.Point(4, 22);
            this.tabPageWebbrowser.Name = "tabPageWebbrowser";
            this.tabPageWebbrowser.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageWebbrowser.Size = new System.Drawing.Size(520, 339);
            this.tabPageWebbrowser.TabIndex = 1;
            this.tabPageWebbrowser.Text = "Output";
            this.tabPageWebbrowser.UseVisualStyleBackColor = true;
            // 
            // propertyGrid
            // 
            this.propertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGrid.Location = new System.Drawing.Point(3, 3);
            this.propertyGrid.Name = "propertyGrid";
            this.propertyGrid.PropertySort = System.Windows.Forms.PropertySort.Alphabetical;
            this.propertyGrid.Size = new System.Drawing.Size(514, 333);
            this.propertyGrid.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.propertyGrid.TabIndex = 1;
            this.propertyGrid.ThemeName = "ControlDefault";
            this.propertyGrid.ToolbarVisible = true;
            // 
            // webBrowserOutput
            // 
            this.webBrowserOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowserOutput.Location = new System.Drawing.Point(3, 3);
            this.webBrowserOutput.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowserOutput.Name = "webBrowserOutput";
            this.webBrowserOutput.Size = new System.Drawing.Size(514, 333);
            this.webBrowserOutput.TabIndex = 1;
            // 
            // PSOutputControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControlContent);
            this.Controls.Add(this.radButtonDeleteItem);
            this.Controls.Add(this.radMultiColumnComboBoxConfiguration);
            this.Controls.Add(this.radButtonNew);
            this.Controls.Add(this.radButtonSave);
            this.Controls.Add(this.radLabelName);
            this.Controls.Add(this.radButtonExecuteModule);
            this.Name = "PSOutputControl";
            this.Size = new System.Drawing.Size(536, 430);
            this.Load += new System.EventHandler(this.GeminiSyncControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceConfiguration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonDeleteItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBoxConfiguration.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBoxConfiguration.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBoxConfiguration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonExecuteModule)).EndInit();
            this.tabControlContent.ResumeLayout(false);
            this.tabPageParameters.ResumeLayout(false);
            this.tabPageWebbrowser.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.propertyGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Telerik.WinControls.UI.RadButton radButtonExecuteModule;
        private System.Windows.Forms.Timer timerCheckRunning;
        private Telerik.WinControls.UI.RadLabel radLabelName;
        private Telerik.WinControls.UI.RadButton radButtonSave;
        private System.Windows.Forms.BindingSource bindingSourceConfiguration;
        private Telerik.WinControls.UI.RadButton radButtonNew;
        private Telerik.WinControls.UI.RadMultiColumnComboBox radMultiColumnComboBoxConfiguration;
        private Telerik.WinControls.UI.RadButton radButtonDeleteItem;
        private System.Windows.Forms.TabControl tabControlContent;
        private System.Windows.Forms.TabPage tabPageParameters;
        private Telerik.WinControls.UI.RadPropertyGrid propertyGrid;
        private System.Windows.Forms.TabPage tabPageWebbrowser;
        private System.Windows.Forms.WebBrowser webBrowserOutput;
    }
}
