﻿using AutomationLibrary.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncStarterModule.Model
{
    public class WebsocketParam : ModuleParamBase
    {
        public string WebsocketUrl { get; set; }
        public string User { get; set; }
        [PasswordPropertyText(true)]
        public string Password { get; set; }

        [Browsable(false)]
        public clsOntologyItem UserItem { get; set; }

        [Browsable(false)]
        public clsOntologyItem GroupItem { get; set; }


        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();
            if (string.IsNullOrEmpty(WebsocketUrl))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "Websocket-Url is invalid!";
                return result;
            }

            try
            {
                var uri = new Uri(WebsocketUrl);
                if (uri == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Websocket-Url is invalid!";
                    return result;
                }
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "Websocket-Url is invalid!";
                return result;
            }
            

            if (string.IsNullOrEmpty(User))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You must provide a User";
                return result;
            }

            if (string.IsNullOrEmpty(Password))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You must provide a Password";
                return result;
            }

            var securityController = new SecurityModule.SecurityController(globals);

            return result;
        }

        public async Task<clsOntologyItem> ValidateLogin(Globals globals)
        {
            var taskResult = await Task.Run<clsOntologyItem>(async() =>
            {
                var result = globals.LState_Success.Clone();

                var securityController = new SecurityModule.SecurityController(globals);

                var resultLogin = await securityController.Login(User, Password);

                result = resultLogin.Result;

                if (!resultLogin.CredentialItems.Any())
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Login was not successful!";
                    return result;
                }

                var credentialItem = resultLogin.CredentialItems.First();

                UserItem = credentialItem.User;
                GroupItem = credentialItem.Group;

                return result;
            });

            return taskResult;
        }

        public override clsOntologyItem GetFunction()
        {
            return AutomationLibrary.Functions.Config.LocalData.Object_Connect_Websocket;
        }

        public WebsocketParam() : base(null)
        {
        }
    }
}
