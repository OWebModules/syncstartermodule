﻿using AutomationLibrary.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncStarterModule.Model
{
    public class ModuleTask
    {
        public Task<clsOntologyItem> Task { get; set; }
        public ModuleItem ModuleItem { get; private set; }

        public bool IsConnectionTask { get; private set; }

        public bool IsConnected { get; set; }

        public OntologyAppDBConnector.IMessageOutput MessageOutput { get; private set; }

        private object locker = new object();

        public BindingList<OutputGridViewItem> Messages { get; set; } = new BindingList<OutputGridViewItem>();

        public ModuleTask(Task<clsOntologyItem> task, ModuleItem moduleItem, OntologyAppDBConnector.IMessageOutput uiOutput, bool isConnectionTask = false)
        {
            IsConnectionTask = isConnectionTask;
            Task = task;
            ModuleItem = moduleItem;
            MessageOutput = uiOutput;
            uiOutput.OutputUIMessage += UiOutput_OutputUIMessage;
        }

        private bool UiOutput_OutputUIMessage(string message, OntologyAppDBConnector.MessageOutputType outputType, DateTime messageStamp)
        {
            var item = new OutputGridViewItem
            {
                Stamp = messageStamp,
                Module = ModuleItem.Name,
                Function = ModuleItem.Function,
                Message = message,
                Type = outputType.ToString()
            };
            lock (locker)
            {
                Messages.Add(item);
            }

            return true;
        }
    }
}
