﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SyncStarterModule.Model
{
    public class ModuleItem
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public string IdFunction { get; set; }
        public string Function { get; set; }
        public UserControl UserControl { get; set; }

        public ModuleItem()
        {
            Id = Guid.NewGuid().ToString();
        }
    }
}
