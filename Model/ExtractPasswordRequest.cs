﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncStarterModule.Model
{
    public class ExtractPasswordRequest
    {
        public string IdUserName { get; private set; }
        public string Password { get; private set; }

        public IMessageOutput MessageOutput { get; set; }

        public ExtractPasswordRequest(string idUserName, string password)
        {
            IdUserName = idUserName;
            Password = password;
        }
    }
}
