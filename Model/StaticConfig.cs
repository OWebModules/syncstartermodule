﻿using SecurityModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncStarterModule.Model
{
    public static class StaticConfig
    {
        public static string MasterPassword { get; set; }
        public static SecurityController SecurityController { get; set; }
    }
}
