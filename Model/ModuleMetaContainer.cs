﻿using AutomationLibrary.Models;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncStarterModule.Model
{
    public  class ModuleMetaContainer
    {
        public List<clsOntologyItem> Modules { get; set; } = new List<clsOntologyItem>();
        public List<ModuleMeta> ModuleMetas { get; set; } = new List<ModuleMeta>();
    }
}
