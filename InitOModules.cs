﻿using AutomationLibrary.Controller;
using AutomationLibrary.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Logging;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SyncStarterModule
{
    public partial class InitOModules : Form
    {

        private Globals globals;
        private Task<clsOntologyItem> taskCreate;
        private string moduleId;

        public InitOModules(Globals globals, string moduleId = null)
        {
            InitializeComponent();
            this.globals = globals;
            Initialize();
            this.moduleId = moduleId;
        }

        private void Initialize()
        {
            radGridViewOutput.DataSource = new BindingList<OutputGridViewItem>();
            UpdateNecessaryOntologies();
        }

        private void UpdateNecessaryOntologies()
        {
            var uiOutput = new UIOutput();

            uiOutput.OutputUIMessageAdded += UiOutput_OutputUIMessageAdded;
            uiOutput.OutputUIMessage += UiOutput_OutputUIMessage;

            var param = new UpdateOntologiesParam
            {
                RootPath = AppDomain.CurrentDomain.BaseDirectory,
                MessageOutput = uiOutput
            };

            var controller = new FileSystemModuleController(param, globals);
            taskCreate = Task.Run<clsOntologyItem>(() => controller.DoWorkAsync());

            var elasticAgent = new Services.ElasticServiceAgent(globals);

            var getModules = elasticAgent.GetModuleMetas(moduleId);
            if (getModules.ResultState.GUID == globals.LState_Error.GUID)
            {
                throw new Exception(getModules.ResultState.Additional1);
            }
        }

        private bool UiOutput_OutputUIMessage(string message, MessageOutputType outputType, DateTime messageStamp)
        {
            if (this.InvokeRequired)
            {
                var deleg = new OutputMessageDelegate(UiOutput_OutputUIMessage);
                this.Invoke(deleg, message, outputType, messageStamp);
            }
            else
            {
                ((BindingList<OutputGridViewItem>)radGridViewOutput.DataSource).Add(new OutputGridViewItem
                {
                    Module = "SyncStarterModule",
                    Function = "Init OModules",
                    Stamp = messageStamp,
                    Type = outputType.ToString(),
                    Message = message
                });
                radGridViewOutput.MasterTemplate.Refresh();

            }
            Application.DoEvents();
            return true;
        }

        private void UiOutput_OutputUIMessageAdded()
        {
            
        }
    }
}
