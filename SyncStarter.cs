﻿using AutomationLibrary.Controller;
using AutomationLibrary.Models;
using OntologyAppDBConnector;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using SyncStarterModule.Attributes;
using SyncStarterModule.Controller;
using SyncStarterModule.Model;
using SyncStarterModule.UserControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using static OntoMsg_Module.Models.UIOutput;
using static SyncStarterModule.Controller.TaskController;

namespace SyncStarterModule
{
    public delegate void SendMessageDelegate(string idClass, string nameClass);
    public partial class SyncStarter : Form
    {

        private Globals globals = new Globals();
        private TaskController taskController;
        private ModuleItem selectedModule;
        private List<IModuleController> controllers = new List<IModuleController>();
        private frmAuthenticate authenticationForm;

        public SyncStarter()
        {
            authenticationForm = new frmAuthenticate(globals);
            authenticationForm.ShowDialog(this);
            
            InitializeComponent();
            NameTransformManager.GetNameTransformProviders(globals);
            taskController = new TaskController(globals);
            taskController.TaskAdded += TaskController_TaskAdded;
            taskController.TaskCompleted += TaskController_TaskCompleted;
            radGridViewOutput.DataBindingComplete += RadGridViewOutput_DataBindingComplete;
            radGridViewModuleList.DataSource = new BindingList<ModuleItem>();
            radGridViewModuleList.CurrentRowChanged += RadGridViewModuleList_CurrentRowChanged;
            radGridViewModuleList.MasterTemplate.ShowHeaderCellButtons = true;
            radGridViewModuleList.MasterTemplate.ShowFilteringRow = true;
            GetModuleItems();
        }

        private void TaskController_TaskCompleted(ModuleItem moduleItem, OntologyClasses.BaseClasses.clsOntologyItem result)
        {

            if (this.InvokeRequired)
            {
                var deleg = new TaskCompletedDelegate(TaskController_TaskCompleted);
                this.Invoke(deleg, moduleItem, result);
            }
            else
            {
                var message = new OutputGridViewItem
                {
                    Type = result.GUID == globals.LState_Error.GUID ? MessageOutputType.Error.ToString() : MessageOutputType.Info.ToString(),
                    Function = moduleItem.Function,
                    Module = moduleItem.Name,
                    Stamp = DateTime.Now,
                    Message = result.Additional1
                };

                ((BindingList<OutputGridViewItem>)radGridViewOutput.DataSource).Add(message);
            }
        }

        private void RadGridViewModuleList_CurrentRowChanged(object sender, CurrentRowChangedEventArgs e)
        {
            RowChanged(e.CurrentRow);
        }

        private void RowChanged(GridViewRowInfo row)
        {

            selectedModule = (ModuleItem)radGridViewModuleList.CurrentRow.DataBoundItem;
            if (selectedModule == null) return;

            splitPanel4.Controls.Clear();
            selectedModule.UserControl.Dock = DockStyle.Fill;
            splitPanel4.Controls.Add(selectedModule.UserControl);

            radGridViewOutput.DataSource = new BindingList<OutputGridViewItem>();
            var taskInList = taskController.GetModuleTask(selectedModule);

            if (taskInList != null)
            {
                foreach (var message in taskInList.Messages)
                {
                    ((BindingList<OutputGridViewItem>)radGridViewOutput.DataSource).Add(message);
                }

            }
        }

        private void RadGridViewOutput_DataBindingComplete(object sender, GridViewBindingCompleteEventArgs e)
        {

        }

        private void TaskController_TaskAdded(ModuleItem moduleItem)
        {

            if (this.InvokeRequired)
            {
                var deleg = new TaskAddedDelegate(TaskController_TaskAdded);
                this.Invoke(deleg, moduleItem);
            }
            else
            {
                //radGridViewOutput.DataSource = null;
                var taskInList = taskController.GetModuleTask(moduleItem);
                if (taskInList != null)
                {
                    //radGridViewOutput.DataSource = taskInList.Messages;
                    if (taskInList.MessageOutput != null)
                    {
                        taskInList.MessageOutput.OutputUIMessageAdded -= UIOutput_OutputUIMessageAdded;
                        taskInList.MessageOutput.OutputUIMessageAdded += UIOutput_OutputUIMessageAdded;
                        taskInList.MessageOutput.OutputUIMessage -= UIOutput_OutputUIMessage;
                        taskInList.MessageOutput.OutputUIMessage += UIOutput_OutputUIMessage;
                    }
                }

            }
        }

        private bool UIOutput_OutputUIMessage(string message, MessageOutputType outputType, DateTime messageStamp)
        {
            if (selectedModule == null) return true;

            if (this.InvokeRequired)
            {
                var deleg = new OutputMessageDelegate(UIOutput_OutputUIMessage);
                this.Invoke(deleg, message, outputType, messageStamp);
            }
            else
            {
                ((BindingList<OutputGridViewItem>)radGridViewOutput.DataSource).Add(new OutputGridViewItem
                {
                    Module = selectedModule.Name,
                    Function = selectedModule.Function,
                    Stamp = messageStamp,
                    Type = outputType.ToString(),
                    Message = message
                });
                radGridViewOutput.MasterTemplate.Refresh();

            }
            Application.DoEvents();
            return true;
        }
        private void UIOutput_OutputUIMessageAdded()
        {
            //if (this.InvokeRequired)
            //{
            //    var deleg = new OutputMessageAddedDelegate(UIOutput_OutputUIMessageAdded);
            //    this.Invoke(deleg);
            //}
            //else
            //{
            //    try
            //    {
            //        radGridViewOutput.MasterTemplate.Refresh();
            //    }
            //    catch (Exception ex)
            //    {

            //        radGridViewOutput.MasterTemplate.Refresh();
            //    }

            //}
        }

        private void GetModuleItems()
        {
            var elasticAgent = new Services.ElasticServiceAgent(globals);
            var moduleMetas = elasticAgent.GetModuleMetas();
            if (moduleMetas.ResultState.GUID == globals.LState_Error.GUID)
            {
                var frmInit = new InitOModules(globals);
                frmInit.ShowDialog(this);
                moduleMetas = elasticAgent.GetModuleMetas();
            }

            var getModelResult = Task.Run<ResultItem<GetSyncStarterModelResult>>(() => elasticAgent.GetSyncStarterModel()).GetAwaiter().GetResult();
            if (getModelResult.ResultState.GUID == globals.LState_Error.GUID)
            {
                MessageBox.Show(getModelResult.ResultState.Additional1, "Error", MessageBoxButtons.OK);
                return;
            }


            var types = AppDomain.CurrentDomain.GetAssemblies().Where(ass => ass.FullName.Contains("AutomationLibrary") || ass.FullName.Contains("SyncStarterModule")).SelectMany(ass => ass.GetTypes()).ToList();

            //var userControls = types.Where(type => type.BaseType == typeof(BaseControlPropertyGrid));

            foreach (var moduleType in types.Where(type => type.BaseType == typeof(BaseController)))
            {
                if (moduleType.GetInterfaces().FirstOrDefault(interf => interf.Name == nameof(IModuleController)) == null) continue;
                var moduleMeta = Activator.CreateInstance(moduleType) as IModuleController;
                if (moduleMeta == null) continue;
                controllers.Add(moduleMeta);
                foreach (var parameterType in moduleMeta.Functions)
                {
                    var parameter = Activator.CreateInstance(parameterType) as IModuleParam;
                    try
                    {
                        var function = parameter.GetFunction();

                        var moduleItem = new ModuleItem
                        {
                            Name = moduleMeta.Module.Name,
                            Id = moduleMeta.Module.GUID,
                            IdFunction = function.GUID,
                            Function = function.Name
                        };

                        if (!moduleMetas.Result.Modules.Any(mod => mod.GUID == moduleMeta.Module.GUID))
                        {
                            var frmInit = new InitOModules(globals, moduleMeta.Module.GUID);
                            frmInit.ShowDialog(this);
                            moduleMetas = elasticAgent.GetModuleMetas();
                        }

                        if (moduleType == typeof(PSOutputParser))
                        {
                            var userControl = new PSOutputControl(moduleItem, globals, taskController, moduleType, parameterType, moduleMetas.Result.ModuleMetas);
                            userControl.ExecuteController += UserControl_ExecuteController;
                            userControl.SendMessage += UserControl_SendMessage;
                            userControl.Dock = DockStyle.Fill;
                            moduleItem.UserControl = userControl;
                            ((BindingList<ModuleItem>)radGridViewModuleList.DataSource).Add(moduleItem);
                        }
                        else if (moduleType == typeof(RabbitMQController))
                        {
                            var userControl = new RabbitMQMessageControl(moduleItem, globals, taskController, moduleType, parameterType);
                            userControl.ExecuteControllerEvent += UserControl_ExecuteController;
                            userControl.SendMessage += UserControl_SendMessage;
                            userControl.Dock = DockStyle.Fill;
                            moduleItem.UserControl = userControl;
                            ((BindingList<ModuleItem>)radGridViewModuleList.DataSource).Add(moduleItem);
                        }
                        else
                        {
                            var userControl = new PropertyGridControl(moduleItem, globals, taskController, moduleType, parameterType, moduleMetas.Result.ModuleMetas);
                            userControl.ExecuteControllerEvent += UserControl_ExecuteController;
                            userControl.SendMessage += UserControl_SendMessage;
                            userControl.Dock = DockStyle.Fill;
                            moduleItem.UserControl = userControl;
                            ((BindingList<ModuleItem>)radGridViewModuleList.DataSource).Add(moduleItem);
                        }
                    }
                    catch (Exception ex)
                    {

                    }


                }


            }

            var moduleItemWebsocket = new ModuleItem
            {
                Name = "WebsocketConnection"
            };
            var userControlWebsocket = (UserControl)Activator.CreateInstance(typeof(WebsocketControl), moduleItemWebsocket, globals, taskController);
            moduleItemWebsocket.UserControl = userControlWebsocket;
            ((BindingList<ModuleItem>)radGridViewModuleList.DataSource).Add(moduleItemWebsocket);

            ModuleList_SelectedIndexChanged(this, new EventArgs());
        }

        private void UserControl_ExecuteController(ModuleParamBase param)
        {
            var controller = controllers.FirstOrDefault(cont => cont.Functions.Contains(param.GetType()));
            var syncConnector = Activator.CreateInstance(controller.GetType(), param, globals);
            var controller1 = (BaseController)syncConnector;
            controller1.DoWorkAsync();
        }

        private void UserControl_SendMessage(string idClass, string nameClass)
        {
            var connectionTask = taskController.GetConnectionTask();

            if (connectionTask != null)
            {
                ((WebsocketControl)connectionTask.ModuleItem.UserControl).SendInterServiceMessage(idClass, nameClass);
            }
        }

        private void ModuleList_SelectedIndexChanged(object sender, EventArgs e)
        {


            //radGridViewOutput.MasterTemplate.Refresh();
        }
        private void SyncStarter_Load(object sender, EventArgs e)
        {
            if (authenticationForm.DialogResult != DialogResult.OK)
            {
                this.Close();
            }
        }
    }
}
