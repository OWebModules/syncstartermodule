﻿using AutomationLibrary.Models;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using SecurityModule.Models;
using SecurityModule.Services;
using SyncStarterModule.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SyncStarterModule.Services
{
    public class ElasticServiceAgent : ElasticBaseAgent
    {

        public Globals Globals
        {
            get { return globals; }
        }

        public ResultItem<ModuleMeta> SaveModuleMeta(ModuleItem moduleItem, object parameter)
        {
            var result = new ResultItem<ModuleMeta>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new ModuleMeta()
            };

            var moduleParam = (IModuleParam)parameter;

            var relationConfig = new clsRelationConfig(globals);


            var moduleMeta = new ModuleMeta
            {
                ModuleConfig = new clsOntologyItem
                {
                    GUID = string.IsNullOrEmpty(moduleParam.ConfigurationId) ? globals.NewGUID : moduleParam.ConfigurationId,
                    Name = moduleParam.ConfigurationName,
                    GUID_Parent = Config.LocalData.Class_ModuleConfig__SyncStarter_.GUID,
                    Type = globals.Type_Object,
                    New_Item = string.IsNullOrEmpty(moduleParam.ConfigurationId)
                },
                Module = new clsOntologyItem
                {
                    GUID = moduleItem.Id,
                    Name = moduleItem.Name,
                    GUID_Parent = Config.LocalData.Class_Module.GUID,
                    Type = globals.Type_Object
                },
                Function = new clsOntologyItem
                {
                    GUID = moduleItem.IdFunction,
                    Name = moduleItem.Function,
                    GUID_Parent = Config.LocalData.Class_Module_Function.GUID,
                    Type = globals.Type_Object
                }
            };

            moduleParam.ConfigurationId = moduleMeta.ModuleConfig.GUID;

            var namespaceString = parameter.GetType().FullName;

            moduleMeta.ParameterNamespace = relationConfig.Rel_ObjectAttribute(moduleMeta.ModuleConfig, Config.LocalData.AttributeType_Parameter_Namespace, namespaceString);

            var dbWriter = new OntologyModDBConnector(globals);

            result.ResultState = dbWriter.SaveObjects(new List<clsOntologyItem> { moduleMeta.ModuleConfig });

            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                result.ResultState.Additional1 = "Error while saving the Config!";
                return result;
            }

            if (moduleMeta.ModuleConfig.New_Item.Value)
            {
                var rel = relationConfig.Rel_ObjectRelation(moduleMeta.ModuleConfig, moduleMeta.Module, Config.LocalData.RelationType_belongs_to);
                result.ResultState = dbWriter.SaveObjRel(new List<clsObjectRel> { rel });
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while saving the relation between Config and Module!";
                    return result;
                }

                rel = relationConfig.Rel_ObjectRelation(moduleMeta.ModuleConfig, moduleMeta.Function, Config.LocalData.RelationType_belongs_to);
                result.ResultState = dbWriter.SaveObjRel(new List<clsObjectRel> { rel });
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while saving the relation between Config and Function!";
                    return result;
                }

                var json = Newtonsoft.Json.JsonConvert.SerializeObject(parameter);
                moduleMeta.JsonAttribute = relationConfig.Rel_ObjectAttribute(moduleMeta.ModuleConfig, Config.LocalData.AttributeType_Json, json);
                var dbReaderAttributes = new OntologyModDBConnector(globals);
                var searchAttributes = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_Object = moduleMeta.ModuleConfig.GUID,
                        ID_AttributeType = Config.LocalData.AttributeType_Json.GUID
                    }
                };
            }
            else
            {
                var searchUserAuthentications = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = moduleMeta.ModuleConfig.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_ModuleConfig__SyncStarter__secured_by_User_Authentication.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_ModuleConfig__SyncStarter__secured_by_User_Authentication.ID_Class_Right
                    }
                };

                var dbReaderUserAuthentications = new OntologyModDBConnector(globals);

                var resultGetUserAuthentications = dbReaderUserAuthentications.GetDataObjectRel(searchUserAuthentications);

                if (resultGetUserAuthentications.GUID == globals.LState_Error.GUID)
                {
                    throw new Exception("No User-Authentications found!");
                }

                var searchUsers = dbReaderUserAuthentications.ObjectRels.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Other,
                    ID_RelationType = Config.LocalData.ClassRel_User_Authentication_secured_by_user.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_User_Authentication_secured_by_user.ID_Class_Right
                }).ToList();

                var dbReaderUsers = new OntologyModDBConnector(globals);

                if (searchUsers.Any())
                {
                    result.ResultState = dbReaderUsers.GetDataObjectRel(searchUsers);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        throw new Exception("No Users of User-Authentications found!");
                    }
                }

                var passwordProperties = parameter.GetType().GetProperties().Where(prop => prop.GetCustomAttributes().FirstOrDefault(att => att is PasswordPropertyTextAttribute) != null);

                foreach (var passwordProp in passwordProperties)
                {
                    var password = (string)passwordProp.GetValue(parameter);
                    if (!string.IsNullOrEmpty(password))
                    {
                        var userRel = dbReaderUsers.ObjectRels.FirstOrDefault(rel => rel.Name_Other == passwordProp.Name);
                        if (userRel != null)
                        {
                            var user = new clsOntologyItem
                            {
                                GUID = userRel.ID_Other,
                                Name = userRel.Name_Other,
                                GUID_Parent = userRel.ID_Parent_Other,
                                Type = userRel.Ontology
                            };

                            var passwordResult = Task.Run<ResultCredentials>(() =>
                            {
                                var passwordGetResult = StaticConfig.SecurityController.GetPassword(user, StaticConfig.MasterPassword);
                                return passwordGetResult;
                            });
                            passwordResult.Wait();
                            result.ResultState = passwordResult.Result.Result;
                            if (result.ResultState.GUID == globals.LState_Error.GUID)
                            {
                                return result;
                            }

                            var idPassword = passwordResult.Result.CredentialItems.FirstOrDefault()?.Password?.ID_Other;
                            var saveResult = Task.Run<ResultItem<PasswordSafeItem>>(() =>
                            {
                                var passwordItem = StaticConfig.SecurityController.SavePassword(user, password, idPassword);

                                return passwordItem;
                            });
                            saveResult.Wait();

                        }
                        else
                        {
                            var userAuthentication = new clsOntologyItem
                            {
                                GUID = globals.NewGUID,
                                Name = passwordProp.Name,
                                GUID_Parent = Config.LocalData.Class_User_Authentication.GUID,
                                Type = globals.Type_Object
                            };

                            var user = new clsOntologyItem
                            {
                                GUID = globals.NewGUID,
                                Name = passwordProp.Name,
                                GUID_Parent = Config.LocalData.Class_user.GUID,
                                Type = globals.Type_Object
                            };

                            var objectsToSave = new List<clsOntologyItem>
                            {
                                userAuthentication,
                                user
                            };

                            result.ResultState = dbWriter.SaveObjects(objectsToSave);

                            if (result.ResultState.GUID == globals.LState_Error.GUID)
                            {
                                result.ResultState.Additional1 = "Error while saving the Authentication and User!";
                                return result;
                            }

                            var relationsToSave = new List<clsObjectRel>();

                            var moduleConfigToUserAuthentication = relationConfig.Rel_ObjectRelation(moduleMeta.ModuleConfig, userAuthentication, Config.LocalData.RelationType_secured_by);
                            relationsToSave.Add(moduleConfigToUserAuthentication);

                            var userAuthenticationTouser = relationConfig.Rel_ObjectRelation(userAuthentication, user, Config.LocalData.RelationType_secured_by);
                            relationsToSave.Add(userAuthenticationTouser);

                            var saveResult = Task.Run<ResultItem<PasswordSafeItem>>(() =>
                            {
                                var passwordItem = StaticConfig.SecurityController.SavePassword(user, password);
                                return passwordItem;
                            });
                            saveResult.Wait();

                            result.ResultState = saveResult.Result.ResultState;

                            if (result.ResultState.GUID == globals.LState_Error.GUID)
                            {
                                return result;
                            }

                            var userAuthenticationToPassword = relationConfig.Rel_ObjectRelation(userAuthentication, saveResult.Result.Result.GetPasswordItem(), Config.LocalData.RelationType_secured_by);
                            relationsToSave.Add(userAuthenticationToPassword);

                            result.ResultState = dbWriter.SaveObjRel(relationsToSave);

                            if (result.ResultState.GUID == globals.LState_Error.GUID)
                            {
                                result.ResultState.Additional1 = "Error while saving the relation between the Module-Config and the User-Authentication or between the User-Authentication and the User!";
                                return result;
                            }
                        }


                    }

                    passwordProp.SetValue(parameter, "");
                }

                var json = Newtonsoft.Json.JsonConvert.SerializeObject(parameter);
                moduleMeta.JsonAttribute = relationConfig.Rel_ObjectAttribute(moduleMeta.ModuleConfig, Config.LocalData.AttributeType_Json, json);
                var dbReaderAttributes = new OntologyModDBConnector(globals);
                var searchAttributes = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_Object = moduleMeta.ModuleConfig.GUID,
                        ID_AttributeType = Config.LocalData.AttributeType_Json.GUID
                    }
                };

                searchAttributes.Add(new clsObjectAtt
                {
                    ID_Object = moduleMeta.ModuleConfig.GUID,
                    ID_AttributeType = Config.LocalData.AttributeType_Parameter_Namespace.GUID
                });

                result.ResultState = dbReaderAttributes.GetDataObjectAtt(searchAttributes);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while reading old json-attributes!";
                    return result;
                }

                var deleteAttributes = dbReaderAttributes.ObjAtts.Select(att => new clsObjectAtt
                {
                    ID_Attribute = att.ID_Attribute
                }).ToList();

                if (deleteAttributes.Any())
                {
                    result.ResultState = dbWriter.DelObjectAtts(deleteAttributes);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while deleting old Json!";
                        return result;
                    }
                }



            }


            result.ResultState = dbWriter.SaveObjAtt(new List<clsObjectAtt>
            {
                moduleMeta.JsonAttribute,
                moduleMeta.ParameterNamespace
            });

            return result;
        }

        public ResultItem<ModuleMetaContainer> GetModuleMetas(string idModule = null)
        {
            var result = new ResultItem<ModuleMetaContainer>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new ModuleMetaContainer()
            };

            var searchModule = new List<clsOntologyItem>();
            if (!string.IsNullOrEmpty(idModule))
            {
                searchModule.Add(
                   new clsOntologyItem
                   {
                       GUID = idModule
                   }
               );
            }
            else
            {
                searchModule.Add(
                   new clsOntologyItem
                   {
                       GUID_Parent = Config.LocalData.Class_Module.GUID
                   }
               );
            }

            var dbReaderModule = new OntologyModDBConnector(globals);

            result.ResultState = dbReaderModule.GetDataObjects(searchModule);

            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                result.ResultState.Additional1 = "Error while getting the Module!";
                return result;
            }

            if (!dbReaderModule.Objects1.Any())
            {
                result.ResultState = globals.LState_Error.Clone();
                result.ResultState.Additional1 = "Module cannot be found!";
                return result;
            }

            result.Result.Modules = dbReaderModule.Objects1;

            var searchModuleToModuleConfig = dbReaderModule.Objects1.Select(mod => 
                   new clsObjectRel
                   {
                       ID_Other = mod.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_ModuleConfig__SyncStarter__belongs_to_Module.ID_RelationType,
                       ID_Parent_Object = Config.LocalData.ClassRel_ModuleConfig__SyncStarter__belongs_to_Module.ID_Class_Left
                   }).ToList();

            var dbReaderModuleConfig = new OntologyModDBConnector(globals);

            result.ResultState = dbReaderModuleConfig.GetDataObjectRel(searchModuleToModuleConfig);

            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                result.ResultState.Additional1 = "Error while getting the ModuleConfig!";
                return result;
            }

            var searchJson = dbReaderModuleConfig.ObjectRels.Select(rel => new clsObjectAtt
            {
                ID_Object = rel.ID_Object,
                ID_AttributeType = Config.LocalData.AttributeType_Json.GUID
            }).ToList();

            var dbReaderJson = new OntologyModDBConnector(globals);

            if (searchJson.Any())
            {
                result.ResultState = dbReaderJson.GetDataObjectAtt(searchJson);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the json!";
                    return result;
                }
            }

            var searchNamespace = dbReaderModuleConfig.ObjectRels.Select(rel => new clsObjectAtt
            {
                ID_Object = rel.ID_Object,
                ID_AttributeType = Config.LocalData.AttributeType_Parameter_Namespace.GUID
            }).ToList();

            var dbReaderParameterNamespace = new OntologyModDBConnector(globals);

            if (searchNamespace.Any())
            {
                result.ResultState = dbReaderParameterNamespace.GetDataObjectAtt(searchNamespace);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the parameter-namespace!";
                    return result;
                }
            }

            var searchFunction = dbReaderModuleConfig.ObjectRels.Select(rel => new clsObjectRel
            {
                ID_Object = rel.ID_Object,
                ID_RelationType = Config.LocalData.ClassRel_ModuleConfig__SyncStarter__belongs_to_Module_Function.ID_RelationType,
                ID_Parent_Other = Config.LocalData.ClassRel_ModuleConfig__SyncStarter__belongs_to_Module_Function.ID_Class_Right
            }).ToList();

            var dbReaderFunction = new OntologyModDBConnector(globals);
            if (searchFunction.Any())
            {
                result.ResultState = dbReaderFunction.GetDataObjectRel(searchFunction);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the functions!";
                    return result;
                }
            }

            var searchUserAuthentications = dbReaderModuleConfig.ObjectRels.Select(rel => new clsObjectRel
            {
                ID_Object = rel.ID_Object,
                ID_RelationType = Config.LocalData.ClassRel_ModuleConfig__SyncStarter__secured_by_User_Authentication.ID_RelationType,
                ID_Parent_Other = Config.LocalData.ClassRel_ModuleConfig__SyncStarter__secured_by_User_Authentication.ID_Class_Right
            }).ToList();

            var dbReaderUserAuthentications = new OntologyModDBConnector(globals);

            if (searchUserAuthentications.Any())
            {
                result.ResultState = dbReaderUserAuthentications.GetDataObjectRel(searchUserAuthentications);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Userauthentications!";
                    return result;
                }
            }

            var searchUsersOfUserAuthentications = dbReaderUserAuthentications.ObjectRels.Select(rel => new clsObjectRel
            {
                ID_Object = rel.ID_Other,
                ID_RelationType = Config.LocalData.ClassRel_User_Authentication_secured_by_user.ID_RelationType,
                ID_Parent_Other = Config.LocalData.ClassRel_User_Authentication_secured_by_user.ID_Class_Right
            }).ToList();

            var dbReaderUserAuthenticationsToUsers = new OntologyModDBConnector(globals);

            if (searchUsersOfUserAuthentications.Any())
            {
                result.ResultState = dbReaderUserAuthenticationsToUsers.GetDataObjectRel(searchUsersOfUserAuthentications);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Users of Userauthentications!";
                    return result;
                }

            }

            result.Result.ModuleMetas.AddRange(from mod in dbReaderModule.Objects1
                                    join config in dbReaderModuleConfig.ObjectRels on mod.GUID equals config.ID_Other
                                   join json in dbReaderJson.ObjAtts on config.ID_Object equals json.ID_Object
                                   join functionItem in dbReaderFunction.ObjectRels on config.ID_Object equals functionItem.ID_Object
                                   join parameterNamespace in dbReaderParameterNamespace.ObjAtts on config.ID_Object equals parameterNamespace.ID_Object into parameterNamspaces
                                   from parameterNamespace in parameterNamspaces.DefaultIfEmpty()
                                   select new ModuleMeta
                                   {
                                       Module = mod,
                                       ModuleConfig = new clsOntologyItem
                                       {
                                           GUID = config.ID_Object,
                                           Name = config.Name_Object,
                                           GUID_Parent = config.ID_Parent_Object,
                                           Type = globals.Type_Object
                                       },
                                       JsonAttribute = json,
                                       ParameterNamespace = parameterNamespace,
                                       Function = new clsOntologyItem
                                       {
                                           GUID = functionItem.ID_Other,
                                           Name = functionItem.Name_Other,
                                           GUID_Parent = functionItem.ID_Parent_Other,
                                           Type = globals.Type_Object
                                       }
                                   });


            foreach (var moduleMeta in result.Result.ModuleMetas)
            {
                moduleMeta.UserAuthentications = dbReaderUserAuthentications.ObjectRels.Where(rel => rel.ID_Object == moduleMeta.ModuleConfig.GUID).Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).ToList();


                moduleMeta.UserAuthenticationsToUsers = (from userAuthentication in dbReaderUserAuthentications.ObjectRels
                                                         join userAuthenticationToUser in dbReaderUserAuthenticationsToUsers.ObjectRels on userAuthentication.ID_Other equals userAuthenticationToUser.ID_Object
                                                         select userAuthenticationToUser).ToList();

            }
            return result;

        }
        public async Task<ResultItem<GetSyncStarterModelResult>> GetSyncStarterModel()
        {
            var taskResult = await Task.Run<ResultItem<GetSyncStarterModelResult>>(() =>
           {
               var result = new ResultItem<GetSyncStarterModelResult>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new GetSyncStarterModelResult()
               };

               var searchModules = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID_Parent = Config.LocalData.Class_Module_of_SyncStarter.GUID
                   }
               };

               var dbReaderModules = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderModules.GetDataObjects(searchModules);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Modules!";
                   return result;
               }

               result.Result.ModuleList = dbReaderModules.Objects1;

               var searchModuleFunctions = result.Result.ModuleList.Select(mod => new clsObjectRel
               {
                   ID_Object = mod.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_Module_of_SyncStarter_offers_Module_Function__SyncStarter_.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Module_of_SyncStarter_offers_Module_Function__SyncStarter_.ID_Class_Right
               }).ToList();

               var dbReaderModuleFunctions = new OntologyModDBConnector(globals);

               if (searchModuleFunctions.Any())
               {
                   result.ResultState = dbReaderModuleFunctions.GetDataObjectRel(searchModuleFunctions);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the ModuleFunctions!";
                       return result;
                   }

                   result.Result.ModulesToModuleFunction = dbReaderModuleFunctions.ObjectRels;
               }

               var searchModuleFunctionsToFunctions = result.Result.ModulesToModuleFunction.Select(rel => new clsObjectRel
               {
                   ID_Object = rel.ID_Other,
                   ID_RelationType = Config.LocalData.ClassRel_Module_Function__SyncStarter__belongs_to_Module_Function.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Module_Function__SyncStarter__belongs_to_Module_Function.ID_Class_Right
               }).ToList();

               var dbReaderModuleFunctionsFunctions = new OntologyModDBConnector(globals);

               if (searchModuleFunctionsToFunctions.Any())
               {
                   result.ResultState = dbReaderModuleFunctionsFunctions.GetDataObjectRel(searchModuleFunctionsToFunctions);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Functions of ModuleFunctions!";
                       return result;
                   }

                   result.Result.ModuleFunctionToFunction = dbReaderModuleFunctionsFunctions.ObjectRels;
               }

               var searchCommandLineRuns = result.Result.ModulesToModuleFunction.Select(rel => new clsObjectRel
               {
                   ID_Object = rel.ID_Other,
                   ID_RelationType = Config.LocalData.ClassRel_Module_Function__SyncStarter__uses_Comand_Line__Run_.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Module_Function__SyncStarter__uses_Comand_Line__Run_.ID_Class_Right
               }).ToList();

               var dbReaderCommandLineRuns = new OntologyModDBConnector(globals);

               if (searchCommandLineRuns.Any())
               {
                   result.ResultState = dbReaderCommandLineRuns.GetDataObjectRel(searchCommandLineRuns);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the CommandLineRuns of ModuleFunctions!";
                       return result;
                   }

                   result.Result.ModuleFunctionToCommandLineRuns = dbReaderCommandLineRuns.ObjectRels;
               }

               return result;
           });

            return taskResult;
        }

        public ResultItem<clsOntologyItem> GetOItem(string idItem, string type)
        {
            var dbReader = new OntologyModDBConnector(globals);

            var result = new ResultItem<clsOntologyItem>
            {
                ResultState = globals.LState_Success.Clone()
            };

            var oItem = dbReader.GetOItem(idItem, type);


            if (oItem.GUID == globals.LState_Error.GUID)
            {
                result.ResultState = globals.LState_Error.Clone();
                result.ResultState.Additional1 = "Error while getting the OItem!";
                return result;
            }

            result.Result = oItem;
            return result;
        }

        public ElasticServiceAgent(Globals globals) : base(globals)
        {
        }
    }
}
